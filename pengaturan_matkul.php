<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Pengaturan Mata Kuliah</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Pengaturan Mata Kuliah</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="">

              <!-- start accordion -->
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Filter</h4>
                          
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <div class="row">
                              <form id="" class="form-horizontal form-label-left">

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Kode Mata Kuliah
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Nama Mata Kuliah
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                  </div>     

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Jenis Mata Kuliah
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Teori</option>
                                          <option value="">Teori dan Praktikum</option>
                                          <option value="">Praktikum</option>
                                          <option value="">Spesial</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>    
                                  
                                  <div class="clearfix"></div>
                                  <div class="ln_solid"></div>

                                  <div class="form-group">
                                    <div class="col-md-12 col-sm-6 col-xs-12 center">
                                       <button class="btn btn-primary" type="reset">Reset</button>
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                              </form>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      
                    </div>
                    <!-- end of accordion -->

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-matkul"><i class="fa fa-plus-circle"></i> Tambah Mata Kuliah</button>


                    <!-- tambah matkul  -->
                    <div class="modal fade tambah-matkul" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-med">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel2">Tambah Mata Kuliah</h4>
                          </div>
                          <div class="modal-body">
                            <form id="" class="form-horizontal form-label-left">

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kode Mata Kuliah
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="" class="form-control" value="">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Mata Kuliah
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="" class="form-control" value="">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kategori
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select id="" class="form-control">
                                    <option value="">Choose..</option>
                                    <option value="">Wajib</option>
                                    <option value="">Pilihan</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jenis Mata Kuliah
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select id="" class="form-control">
                                    <option value="">Choose..</option>
                                    <option value="">Teori</option>
                                    <option value="">Teori dan Praktikum</option>
                                    <option value="">Praktikum</option>
                                    <option value="">Spesial</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">SKS Teori
                                </label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                  <input type="text" id="" class="form-control" value="">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">SKS Praktek
                                </label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                  <input type="text" id="" class="form-control" value="">
                                </div>
                              </div>

                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                            </form>
                          </div>
                          <div class="modal-footer center ">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn btn-primary">Simpan</button>
                          </div>

                        </div>
                      </div>
                    </div>


                    <!-- edit matkul  -->
                    <div class="modal fade edit-matkul" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-med">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel2">Edit Mata Kuliah</h4>
                          </div>
                          <div class="modal-body">
                            <form id="" class="form-horizontal form-label-left">

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kode Mata Kuliah
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="" class="form-control" value="MK-2002">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Mata Kuliah
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="" class="form-control" value="Kesegaran Jasmani">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kategori
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select id="" class="form-control">
                                    <option value="">Choose..</option>
                                    <option selected value="">Wajib</option>
                                    <option value="">Pilihan</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jenis Mata Kuliah
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select id="" class="form-control">
                                    <option value="">Choose..</option>
                                    <option value="">Teori</option>
                                    <option value="">Teori dan Praktikum</option>
                                    <option selected value="">Praktikum</option>
                                    <option value="">Spesial</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">SKS Teori
                                </label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                  <input type="text" id="" class="form-control" value="2">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">SKS Praktek
                                </label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                  <input type="text" id="" class="form-control" value="1">
                                </div>
                              </div>

                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                            </form>
                          </div>
                          <div class="modal-footer center ">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn btn-primary">Simpan</button>
                          </div>

                        </div>
                      </div>
                    </div>

                    <!-- hapus matkul  -->
                    <div class="modal fade hapus-matkul" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-med">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel2">Hapus Mata Kuliah</h4>
                          </div>

                          <div class="modal-body center">
                            <p>
                              Apakah Anda yakin ingin menghapus data di bawah ini?
                              <div>
                                <strong>AIK 1 (Kemanusaiaan dan Keimanan)</strong>
                              </div>
                            </p>
                          </div>

                          <div class="modal-footer center ">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn btn-primary">Hapus</button>
                          </div>

                        </div>
                      </div>
                    </div>

                    <div class="mt15">

                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Mata Kuliah</th>
                          <th>Nama Mata Kuliah</th>
                          <th class="no-sort">SKS</th>
                          <th>Jenis</th>
                          <th>Kategori</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php for ($i = 0; $i < 30; $i++){ 
                      $j=$i+1;
                      $k=$i+2019;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>MPK-120'.$i.'</td>
                          <td>Aplikasi Sistem Komputer Farmasi</td>
                          <td>2/0</td>
                          <td>Teori</td>
                          <td>Wajib</td>
                          <td>
                            <a href="silabus.php" class="btn btn-xs btn-success"><i class="fa fa-book"></i> Silabus</a>
                            <button data-toggle="modal" data-target=".edit-matkul" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                            <button data-toggle="modal" data-target=".hapus-matkul" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                          </td>
                        </tr>';
                      }?>
                      </tbody>
                    </table>

                  </div>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>