<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kegiatan PMB - Tahun 2019</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="pmb_kegiatan.php">Kegiatan PMB</a></li>
                      <li class="breadcrumb-item active" aria-current="page">2019</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <a class="btn btn-primary" href="tambah_gel.php"><i class="fa fa-plus-circle"></i> Tambah Gelombang</a>

            <div class="row">

            <?php for ($i = 0; $i < 9; $i++){ 
              $j=$i+1;
               echo '
              <div class="col-md-4 col-sm-4 col-xs-12">
                  <span class="list-tahun">
                    <span class="tahun-kegiatan">Gelombang ' .$j. '</span>
                    <span class="col-md-6 col-sm-6 kolom">
                      <span class="kolom-head">Pendaftar</span>
                      <span class="kolom-jumlah">320</span>
                    </span>
                    <span class="col-md-6 col-sm-6 kolom lulus">
                      <span class="kolom-head">Lulus</span>
                      <span class="kolom-jumlah">120</span>
                    </span>
                    <span class="edit-gel">
                    <span class="col-md-4 col-sm-4"><a class="btn btn-warning btn-sm" title="View" href="detail_gel.php"><i class="fa fa-eye"></i> View</a></span>
                    <span class="col-md-4 col-sm-4"><a class="btn btn-primary btn-sm" title="Edit" href="edit_gel.php"><i class="fa fa-cog"></i> Edit</a></span>
                    <span class="col-md-4 col-sm-4"><a class="btn btn-info btn-sm" title="List Peserta" href="peserta_gel.php"><i class="fa fa-group"></i> List</a></span>
                    </span>
                  </span>
              </div>
              ';

             } ?>

            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
