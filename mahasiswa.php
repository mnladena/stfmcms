<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Mahasiswa - Tahun Pendaftaran</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="mahasiswa.php">Mahasiswa</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kemahasiswaan</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="mt15">

                <table id="" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tahun Masuk</th>
                      <th class="no-sort">Jumlah Mahasiswa</th>
                      <th class="no-sort">Program Studi</th>
                      <th class="no-sort">Jenis Daftar</th>
                      <th class="no-sort">Action</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tbody>
                      <?php for ($i = 0; $i < 30; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>2019</td>
                          <td>230</td>
                          <td>
                            <div>D3 Farmasi : 100</div>
                            <div>S1 Farmasi : 130</div>
                          </td>
                          <td>
                            <div>Reguler : 200</div>
                            <div>Kaderisasi : 10</div>
                            <div>Berprestasi : 20</div>
                          </td>
                          <td>
                            <a href="detail_mahasiswa.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                          </td>
                        </tr>';
                      }?>
                    </tbody>

                </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>