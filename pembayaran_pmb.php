<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Pebayaran PMB</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="">Keuangan</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Pembayaran PMB</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="">

              <!-- start accordion -->
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Filter</h4>
                          
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <div class="row">
                              <form id="" class="form-horizontal form-label-left">

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">NIRM
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">NAMA
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Jenjang Pendidikan
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>


                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Status Pembayaran
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>     

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                  
                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Tanggal Bayar
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class='input-group date'>
                                            <input required="required" type='text' class="form-control"  id='datetimepicker6' value="20/08/1995"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Tanggal Verifikasi
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class='input-group date'>
                                            <input required="required" type='text' class="form-control"  id='datetimepicker7' value="20/08/1995"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Jenis Pembayaran
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Nominal Bayar
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Status Valid
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="checkbox">
                                          <label>
                                            <input type="checkbox" class="flat" checked="checked"> Sembunyikan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                  </div>    
                                  
                                  <div class="clearfix"></div>
                                  <div class="ln_solid"></div>

                                  <div class="form-group">
                                    <div class="col-md-12 col-sm-6 col-xs-12 center">
                                       <button class="btn btn-primary" type="reset">Reset</button>
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                              </form>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      
                    </div>
                    <!-- end of accordion -->

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".create-interview"><i class="fa fa-plus-circle"></i> Tambah Versi</button>

                    <!-- create  -->
            <div class="modal fade create-interview" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Versi</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label" for="">Waktu Pembuatan
                        </label>
                        <div class="">
                          <span class="inp-text">Kamis, 4 juli 2019</span> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Nama Versi
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Deskripsi
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5></textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

              <div class="mt15">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Data Diri</th>
                          <th>Jenis Pendaftaran</th>
                          <th class="no-sort">Nominal Pembayaran</th>
                          <th>Tanggal Bayar</th>
                          <th>Tanggal Verifikasi</th>
                          <th class="no-sort">Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php for ($i = 0; $i < 30; $i++){ 
                      $j=$i+1;
                      $k=$i+2019;
                      echo '
                        <tr>
                          <td>
                            '.$j.'
                          </td>
                          <td>
                            Registrasi
                          </td>
                          <td>
                            Rp 500.000
                          </td>
                          <td>
                            20/08/2019
                          </td>
                          <td>
                            20/08/2019
                          </td>
                          <td>
                            Valid
                          </td>
                          <td>
                            <a href="" class="btn btn-xs btn-success" data-toggle="modal" data-target=".data-valid"><i class="fa fa-check"></i> Valid</a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            '.$j.'
                          </td>
                          <td>
                            Registrasi
                          </td>
                          <td>
                            Rp 500.000
                          </td>
                          <td>
                            20/08/2019
                          </td>
                          <td>
                            20/08/2019
                          </td>
                          <td>
                            Belum Valid
                          </td>
                          <td>
                            <a href="" class="btn btn-xs btn-danger" data-toggle="modal" data-target=".data-notvalid"><i class="fa fa-exclamation"></i> Not Valid</a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            '.$j.'
                          </td>
                          <td>
                            Registrasi
                          </td>
                          <td>
                            Rp 500.000
                          </td>
                          <td>
                            20/08/2019
                          </td>
                          <td>
                            20/08/2019
                          </td>
                          <td>
                            Pengecekan
                          </td>
                          <td>
                            <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".data-cek"><i class="fa fa-info"></i> Check</a>
                          </td>
                        </tr>';
                      }?>
                      </tbody>
                    </table>
                  </div>

                  <!-- modal valid -->
                  <div class="modal fade data-valid" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Konfirmasi Submit Pembayaran</h4>
                        </div>
                        <div class="modal-body">
                          
                          <!-- message -->
                          <div class="alert alert-success center" role="alert">
                            VALID
                          </div>

                          <table class="form-preview table table-striped">
                            <tr>
                              <td colspan="2">
                                <div class="nama-mahasiswa">Rini Handayani - 0345431</div>
                              </td>
                            </tr>
                            <tr>
                              <td>Biaya Pendaftaran</td>
                              <td>Rp 1.100.000</td>
                            </tr>
                            <tr>
                              <td>Id Unik</td>
                              <td>#U35</td>
                            </tr>
                            <tr>
                              <td>Nilai yang harus ditransfer</td>
                              <td><b>Rp 13.500.056</b></td>
                            </tr>
                            <tr>
                              <td>Bank Tujuan</td>
                              <td>BCA</td>
                            </tr>
                            <tr>
                              <td>Bukti Bayar</td>
                              <td><img src="images/struk.jpg"></td>
                            </tr>
                          </table>

                        </div>

                        <div class="modal-footer center ">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                        </div>

                </div>
              </div>
            </div>

            <!-- modal belum valid -->
                  <div class="modal fade data-notvalid" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Konfirmasi Submit Pembayaran</h4>
                        </div>
                        <div class="modal-body">
                          
                          <!-- message -->
                          <div class="alert alert-danger center" role="alert">
                            BELUM VALID
                          </div>

                          <table class="form-preview table table-striped">
                            <tr>
                              <td colspan="2">
                                <div class="nama-mahasiswa">Rini Handayani - 0345431</div>
                              </td>
                            </tr>
                            <tr>
                              <td>Biaya Pendaftaran</td>
                              <td>Rp 1.100.000</td>
                            </tr>
                            <tr>
                              <td>Id Unik</td>
                              <td>#U35</td>
                            </tr>
                            <tr>
                              <td>Nilai yang harus ditransfer</td>
                              <td><b>Rp 13.500.056</b></td>
                            </tr>
                            <tr>
                              <td>Bank Tujuan</td>
                              <td>BCA</td>
                            </tr>
                            <tr>
                              <td>Bukti Bayar</td>
                              <td><img src="images/struk.jpg"></td>
                            </tr>
                            <tr>
                              <td>Catatan</td>
                              <td>
                                <span class="text-danger">Pada bukti transfer berbeda dengan yang harus ditransfer pada sistem</span>
                              </td>
                            </tr>
                          </table>

                        </div>

                        <div class="modal-footer center ">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                        </div>

                </div>
              </div>
            </div>

            <!-- modal cek -->
                  <div class="modal fade data-cek" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Konfirmasi Submit Pembayaran</h4>
                        </div>
                        <div class="modal-body">

                          <table class="form-preview table table-striped">
                            <tr>
                              <td colspan="2">
                                <div class="nama-mahasiswa">Rini Handayani - 0345431</div>
                              </td>
                            </tr>
                            <tr>
                              <td>Biaya Pendaftaran</td>
                              <td>Rp 1.100.000</td>
                            </tr>
                            <tr>
                              <td>Id Unik</td>
                              <td>#U35</td>
                            </tr>
                            <tr>
                              <td>Nilai yang harus ditransfer</td>
                              <td><b>Rp 13.500.056</b></td>
                            </tr>
                            <tr>
                              <td>Bank Tujuan</td>
                              <td>BCA</td>
                            </tr>
                            <tr>
                              <td>Bukti Bayar</td>
                              <td><img src="images/struk.jpg"></td>
                            </tr>
                            <tr>
                              <td>Hasil Verifikasi</td>
                              <td>
                                <select id="" class="form-control">
                                  <option value="">Choose..</option>
                                  <option selected value="1">Valid</option>
                                  <option value="2">Belum Valid</option>
                                </select>
                              </td>
                            </tr>
                            <tr>
                              <td>Catatan</td>
                              <td>
                                <textarea id="message" class="form-control" name="" rows=5></textarea>
                              </td>
                            </tr>
                          </table>

                        </div>

                        <div class="modal-footer center ">
                          <button type="submit" class="btn btn-success">Submit</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                        </div>

                </div>
              </div>
            </div>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>