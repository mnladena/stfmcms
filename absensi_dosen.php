<?php include "include/head.php" ?>
  
  <body>

    <!-- header -->
    <div class="header-absensi text-center col-md-12 col-sm-12">
      <div class="header-absensi-logo">
        <img src="images/logo.png" alt="">
      </div>
      <span>
        Sekolah Tinggi Farmasi Muhammadiyah Tangerang
      </span>

    </div>
    <div class="clearfix"></div>
    <!-- header -->

   <div class="container body">
      <div class="container-page">

        <div class="page-title">

            <div class="title_left">
              <h3>Absensi Dosen</h3>
            </div>

        </div>

        <div class="keterangan-dosen">
          
          <div class="info-dosen">
            <table class="table table_info table_info-noborder">
              <tr>
                <td>Tahun Ajaran</td>
                <td>:</td>
                <td>2019/2020</td>
              </tr>
              <tr>
                <td>Semester</td>
                <td>:</td>
                <td>Ganjil</td>
              </tr>
              <tr>
                <td>Hari/Tanggal</td>
                <td>:</td>
                <td>Senin, 9/12/2020</td>
              </tr>
            </table>
          </div>

          <div class="dosen-time">
            <span id="jam">10</span>:<span id="menit">30</span>:<span id="detik">00</span>
          </div>

          <div class="clearfix"></div>

        </div>

        <div class="mt15">
          
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Mata Kuliah</th>
                <th>SKS</th>
                <th>Kelas</th>
                <th>Pertemuan</th>
                <th>Dosen</th>
                <th>Waktu</th>
                <th>Checkin</th>
                <th>Checkout</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>1</td>
                <td>
                  <div>MPK-1202</div>
                  <div>AIK1 (kemanusiaan dan Keimanan)</div>
                </td>
                <td>2/0</td>
                <td>14A </td>
                <td>3</td>
                <td>Muhammad Sanusi Lc</td>
                <td>08.00 - 09.00</td>
                <td>-</td>
                <td>-</td>
                <td>
                  <span class="text-danger">Belum Hadir</span>
                </td>
                <td>
                  <button class="btn btn-sm btn-info" data-toggle="modal" data-target=".status-check"><i class="fa fa-check"></i> Checkin</button>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>
                  <div>MPK-1204</div>
                  <div>Fisika Dasar</div>
                </td>
                <td>2/1</td>
                <td>16A </td>
                <td>2</td>
                <td>Rini Oktavia</td>
                <td>08.00 - 09.30</td>
                <td>-</td>
                <td>-</td>
                <td>
                  <span class="text-danger">Belum Hadir</span>
                </td>
                <td>
                  <button class="btn btn-sm btn-info" data-toggle="modal" data-target=".status-check"><i class="fa fa-check"></i> Checkin</button>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>
                  <div>MPK-1201</div>
                  <div>Aplikasi Sistem Komputer Farmasi</div>
                </td>
                <td>2/0</td>
                <td>15B </td>
                <td>2</td>
                <td>Wildan Hamid</td>
                <td>09.00 - 10.00</td>
                <td>08.50</td>
                <td>-</td>
                <td>
                  <span class="text-success">Hadir</span>
                </td>
                <td></td>
              </tr>
            </tbody>
            
          </table>

        </div>

        <!-- checkin  -->
        <div class="modal fade status-check" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-med">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Absensi Dosen</h4>
              </div>

              <div class="modal-body">

                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="text-center qrimg">
                      <img src="images/qr.png">
                    </div>
                    <div class="text-center">Silahkan Scan QR Code di atas</div>
                  </div>

                  <div class="col-sm-6 col-md-6">
                    <div class="info-list">
                      <div class="info-label">Kode MK</div>
                      <div class="info-data">MPK-12101</div>

                      <div class="info-label">Nama MK</div>
                      <div class="info-data">AIK 1 (Kemanusiaan dan Keimanan)</div>

                      <div class="info-label">SKS</div>
                      <div class="info-data">2/0</div>

                      <div class="info-label">Kelas</div>
                      <div class="info-data">14A</div>

                      <div class="info-label">Pertemuan</div>
                      <div class="info-data">Pertemuan ke-3</div>

                      <div class="info-label">Dosen</div>
                      <div class="info-data">Muhammad Sanusi Lc</div>

                      <div class="info-label">Waktu</div>
                      <div class="info-data">08.00 0 09.00</div>

                      <button class="btn btn-block btn-success">Selesai</button>

                    </div>
                  </div>
                </div>
                
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
  
<?php include "include/footer.php" ?>
