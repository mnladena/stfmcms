<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Report Penilaian PMB - Report Ujian</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="">

              <!-- start accordion -->
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Filter</h4>
                          
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <div class="row">
                              <form id="" class="form-horizontal form-label-left">

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="">Tahun
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option selected value="">2019</option>
                                          <option value="">2020</option>
                                          <option value="">2021</option>
                                          <option value="">2022</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="">Gelombang
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option selected value="">1</option>
                                          <option value="">2</option>
                                          <option value="">3</option>
                                          <option value="">4</option>
                                        </select>
                                      </div>
                                    </div>

                                    

                                  </div>     

                                  
                                  <div class="clearfix"></div>
                                  <div class="ln_solid"></div>

                                  <div class="form-group">
                                    <div class="col-md-12 col-sm-6 col-xs-12 center">
                                       <button class="btn btn-primary" type="reset">Reset</button>
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                              </form>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      
                    </div>
                    <!-- end of accordion -->

                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No Peserta</th>
                          <th>Nama Peserta</th>
                          <th>Jumlah Benar</th>
                          <th>Nilai</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php for ($i = 0; $i < 30; $i++){ 
                      $j=$i+1;
                      $k=$i+2019001;
                      echo '
                        <tr>
                          <td>'.$k.'</td>
                          <td>
                            <div>Salahuddin Yusuf</div>
                          </td>
                          <td>
                            100
                          </td>
                          <td>
                            100.00
                          </td>
                        </tr>';
                      }?>
                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

        <div class="modal fade biodata-peserta" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Biodata Peserta</h4>
                  </div>
                  <div class="modal-body">
                    <div class="img-peserta"><img src="images/img.jpg" alt=""></div>

                    <div class="box-peserta">

                    <table class="table table-striped">
                      <tr>
                        <td>NIRM</td>
                        <td>:</td>
                        <td>034511</td>
                      </tr>
                      <tr>
                        <td>Nama Peserta</td>
                        <td>:</td>
                        <td>Jonny Iskadar</td>
                      </tr>
                      <tr>
                        <td>Asal Sekolah</td>
                        <td>:</td>
                        <td>SMAN 1 Tangerang</td>
                      </tr>
                      <tr>
                        <td>Jenjang</td>
                        <td>:</td>
                        <td>S1 Farmasi</td>
                      </tr>
                      <tr>
                        <td>Jenis Daftar</td>
                        <td>:</td>
                        <td>Reguler</td>
                      </tr>
                    </table>

                    <div class="ln_solid"></div>
                    
                    <div class="col-md-12 col-sm-12 col-xs-12 center">
                        <a href="interview_aik.php" class="btn btn-default">Interview AIK</a>
                        <a href="interview_akademik.php" class="btn btn-default">Interview Akademik</a>
                        <a href="interview_keuangan.php" class="btn btn-default">Interview Keuangan</a>
                    </div>

                    <div class="clearfix"></div>

                  </div>
                </div>
              </div>
            </div>

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>