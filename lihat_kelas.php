<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kartu Rencana Studi S1</h3>
                <h4>Mata Kuliah Semester 5 - Obat Bahan Alami</h4>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">KRS - S1</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            

            <div class="row">
              <div class="col-md-9 col-sm-9 col-xs-9">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-kelas"><i class="fa fa-plus-circle"></i> Tambah Kelas</button>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 right">
                <a href="lihat_krs.php" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
              </div>
            </div>

            <div class="mt15">

            <!-- tambah kelas  -->
            <div class="modal fade tambah-kelas" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Kelas</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Kelas
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kuota
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit kelas  -->
            <div class="modal fade edit-kelas" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Kelas</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Kelas
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control" value="Obat Bahan Alami">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kuota
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" class="form-control" value="60">
                        </div>
                      </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Edit</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- delete -->
          <div class="modal fade hapus-matkul" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Hapus Kelas</h4>
                  </div>
                  <div class="modal-body text-center">
                    <div class="alert alert-danger mb15">Kelas tidak bisa dihapus karena masih ada mahasiswa</div>
                    <div>Apakah Anda yakin ingin menghapus Kelas? <b>Mata kuliah Obat Bahan Alam (Wajib)</b></div>
                    <div>Jumlah Mahasiswa : 55</div>

                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Hapus</button>
                  </div>

                </div>
              </div>
            </div>


                    <table id="" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Kelas</th>
                          <th>Kuota</th>
                          <th>Jumlah Mahasiswa</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>


                      <tbody>

                      <?php for ($i = 0; $i < 6; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>Kelas A</td>
                          <td>60</td>
                          <td>55</td>
                          <td>
                            <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".hapus-matkul"><i class="fa fa-edit"></i> Edit</button>
                            <a href="lihat_mahasiswa.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Mahasiswa</a>
                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target=".hapus-matkul"><i class="fa fa-trash"></i> Hapus</button>
                          </td>
                        </tr>';
                      }?>

                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>