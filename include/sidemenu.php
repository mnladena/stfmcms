<!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Halaman Utama</a></li>

                  <li><a><i class="fa fa-users"></i> PMB <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="pmb_kegiatan.php">Kegiatan PMB</a></li>
                      <li><a>Penilaian PMB<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                          <li class="sub_menu"><a href="absensi_ujian.php">Absensi Ujian</a>
                            </li>
                            <li><a href="proses_interview.php">Proses Interview</a>
                            </li>
                            <li><a href="soal_ujian.php">Soal Ujian</a>
                            </li>
                            <li><a href="soal_interview.php">Soal Interview</a>
                            </li>
                          </ul>
                        </li>
                        <li><a>Report Penilaian PMB<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                          <li class="sub_menu"><a href="report_ujian.php">Report Ujian</a>
                            </li>
                            <li><a href="report_interview.php">Report Interview</a>
                            </li>
                            <li><a href="report_penilaian.php">Report Penilaian PMB</a>
                            </li>
                          </ul>
                        </li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-suitcase"></i> Mahasiswa <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="mahasiswa.php">Kemahasiswaan</a></li>
                      <li><a href="kelas.php">Kelas</a></li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-university"></i> Akademik <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a>KRS <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="krs_s1.php">S1</a></li>
                          <li><a href="pengaturan_krs.php">Pengaturan KRS</a></li>
                        </ul>
                      </li>
                      <li><a href="jadwal_kuliah.php">Jadwal Kuliah</a></li>
                      <li><a href="perkuliahan.php">Perkuliahan</a></li>
                      <li><a href="absensi.php">Absensi</a></li>
                      <li><a href="#">Ujian</a></li>
                      <li><a href="#">KHS</a></li>
                      <li><a href="#">Bimbingan Akademik</a></li>
                      <li><a href="#">PraKerja</a></li>
                      <li><a href="#">Sidang</a></li>
                      <li><a>Layanan <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="krs_s1.php">Menu 1</a></li>
                          <li><a href="pengaturan_krs.php">Menu 2</a></li>
                        </ul>
                      </li>
                      <li><a>Pengaturan <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="pengaturan_matkul.php">Mata Kuliah</a></li>
                          <li><a href="pengaturan_kurikulum.php">Kurikulum</a></li>
                          <li><a href="pengaturan_nilai.php">Penilaian</a></li>
                          <li><a href="pengaturan_krs.php">Ruangan</a></li>
                          <li><a href="pengaturan_prodi.php">Program Studi</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-money"></i> Keuangan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a>Pembayaran <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="pembayaran_pmb.php">PMB</a></li>
                          <li><a href="pembiayaan_kuliah.php">Perkuliahan</a></li>
                          <li><a href="pembiayaan_kuliah.php">Sidang</a></li>
                          <li><a href="pembiayaan_kuliah.php">Kegiatan Mahasiswa</a></li>
                        </ul>
                      </li>
                      <li><a>Pembiayaan <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="biaya_kuliah.php">Perkuliahan</a></li>
                          <li><a href="biaya_support.php">Pendukung Perkuliahan</a></li>
                          <li><a href="Biaya_diskon.php">Diskon</a></li>
                          <li><a href="biaya_event.php">Kegiatan Kampus</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-user"></i> User Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                          <li><a href="pengguna.php">Pengguna</a></li>
                          <li><a href="grup_akses.php">Grup Akses</a></li>
                        </ul>
                  </li>
                  <li><a><i class="fa fa-hand-paper-o"></i> Log Aktifitas <span class="fa fa-chevron-down"></span></a></li>
                  <li><a><i class="fa fa-cog"></i> Konfigurasi <span class="fa fa-chevron-down"></span></a></li>

                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

          </div>
        </div>