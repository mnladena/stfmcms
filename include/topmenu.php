<!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">Admin STFM
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="profil.php"> Profile</a></li>
                    <li><a href="login.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="message">
                          Jadwal kuliahmu sudah dipublish, silahkan melihatnya di menu jadwal
                        </span>
                        <span>
                          <span class="time">12/12/2019 - 08.30</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="message">
                          Jadwal kuliahmu sudah dipublish, silahkan melihatnya di menu jadwal
                        </span>
                        <span>
                          <span class="time">12/12/2019 - 08.30</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="message">
                          Jadwal kuliahmu sudah dipublish, silahkan melihatnya di menu jadwal
                        </span>
                        <span>
                          <span class="time">12/12/2019 - 08.30</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="message">
                          Jadwal kuliahmu sudah dipublish, silahkan melihatnya di menu jadwal
                        </span>
                        <span>
                          <span class="time">12/12/2019 - 08.30</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a href="notifikasi.php">
                          <strong>Lihat Semua</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->