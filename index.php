<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Halaman Utama</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Informasi Pendaftaran 2019/2020</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                 <p> Memasuki tahun akademik 2019/2020, UHAMKA membuka pendaftaran dan penerimaan mahasiswa baru untuk Program Sarjana (S1), Diploma (D3 & D4) dan Pascasarjana (S2) melalui Jalur Tes, Jalur Tanpa Tes, dan Jalur Penelusuran Minat dan Kemampuan (PMDK).</p>

                  <p>Untuk pendaftaran mahasiswa baru UHAMKA, calon mahasiswa dapat mengunjungi tempat pendaftaran di setiap kampus UHAMKA atau melalui pendaftaran secara online melalui tautan ini.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
