<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Pembiayaan Perkuliahan</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <div class="" role="tabpanel" data-example-id="togglable-tabs">

              <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">D3 Farmasi</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">S1 Farmasi</a>
                </li>
              </ul>

              <div id="myTabContent" class="tab-content">

                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="d3-tab">
                  
                  <div class="mt15">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-tahun"><i class="fa fa-plus-circle"></i> Tambah Tahun</button>

                    <div class="mt15">
                      <table id="" class="table table-striped table-bordered datatable">
                        <thead>
                          <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Tahun</th>
                            <th class="no-sort" rowspan="2">BOP</th>
                            <th colspan="4">BPP</th>
                            <th class="no-sort" rowspan="2">Action</th>
                          </tr>
                          <tr>
                            <td class="no-sort">Gelombang 1</td>
                            <td class="no-sort">Gelombang 2</td>
                            <td class="no-sort">Gelombang 3</td>
                            <td class="no-sort">Gelombang 4</td>
                          </tr>
                        </thead>


                        <tbody>
                        <?php for ($i = 0; $i < 30; $i++){ 
                        $j=$i+1;
                        $k=$i+2019;
                        echo '
                          <tr>
                            <td>
                              '.$j.'
                            </td>
                            <td>
                              2019
                            </td>
                            <td>
                              Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                              -
                            </td>
                            <td>
                              <a href="detail_biaya_kuliah.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                            </td>
                          </tr>';
                        }?>
                        </tbody>
                      </table>
                    </div>
                    

                  
                  </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="s1-tab">
                  
                    <div class="mt15">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-tahun"><i class="fa fa-plus-circle"></i> Tambah Tahun</button>

                    <div class="mt15">
                      <table id="" class="table table-striped table-bordered datatable">
                        <thead>
                          <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Tahun</th>
                            <th class="no-sort" rowspan="2">BOP</th>
                            <th colspan="4">BPP</th>
                            <th class="no-sort" rowspan="2">Action</th>
                          </tr>
                          <tr>
                            <td class="no-sort">Gelombang 1</td>
                            <td class="no-sort">Gelombang 2</td>
                            <td class="no-sort">Gelombang 3</td>
                            <td class="no-sort">Gelombang 4</td>
                          </tr>
                        </thead>


                        <tbody>
                        <?php for ($i = 0; $i < 30; $i++){ 
                        $j=$i+1;
                        $k=$i+2019;
                        echo '
                          <tr>
                            <td>
                              '.$j.'
                            </td>
                            <td>
                              2019
                            </td>
                            <td>
                              Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                              -
                            </td>
                            <td>
                              <a href="detail_biaya_kuliah.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                            </td>
                          </tr>';
                        }?>
                        </tbody>
                      </table>
                    </div>
                    

                  
                  </div>

                </div>

                
            </div>

          </div>

          <div class="modal fade tambah-tahun" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Tahun</h4>
                  </div>
                  <div class="modal-body">
                      <label for="heard">Tahun:</label>
                      <select id="heard" class="form-control" required>
                        <option value="">Choose..</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                      </select>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah</button>
                  </div>

                </div>
              </div>
            </div>
          

            
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>