<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Pengaturan Program Studi</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Program Studi</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-prodi"><i class="fa fa-plus-circle"></i> Tambah Program Studi</button>

            <div class="mt15">

            <!-- tambah Prodi  -->
            <div class="modal fade tambah-prodi" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Program Studi</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kode
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Program Studi
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jenjang
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">D3 </option>
                            <option value="">S1 </option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jumlah Semester
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit Prodi  -->
            <div class="modal fade edit-prodi" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Program Studi</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kode
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" class="form-control" value="MK-2001">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Program Studi
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control" value="D3 Farmasi">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jenjang
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option selected value="">D3 </option>
                            <option value="">S1 </option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jumlah Semester
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" class="form-control" value="6">
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            

                    <table id="" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode</th>
                          <th>Nama Program Studi</th>
                          <th>Jenjang</th>
                          <th>Semester</th>
                          <th>Kepala Program Studi</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>D3</td>
                          <td>D3 Farmasi</td>
                          <td>D3</td>
                          <td>6</td>
                          <td>Meta Safitri S.Farm M.Sc APT</td>
                          <td>
                              <a href="detail_kaprodi.php" class="btn btn-xs btn-warning"><i class="fa fa-user"></i> Detail Kaprodi</a>
                              <button data-toggle="modal" data-target=".edit-prodi" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                              <button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                          </td>
                        </tr>

                        <tr>
                          <td>2</td>
                          <td>S1</td>
                          <td>S1 Farmasi</td>
                          <td>S1</td>
                          <td>8</td>
                          <td>Dina Pratiwi S.Farm M.Si</td>
                          <td>
                              <a href="detail_kaprodi.php" class="btn btn-xs btn-warning"><i class="fa fa-user"></i> Detail Kaprodi</a>
                              <button data-toggle="modal" data-target=".edit-prodi" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                              <button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                          </td>
                        </tr>

                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>