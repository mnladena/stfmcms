<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Soal Interview</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <div class="" role="tabpanel" data-example-id="togglable-tabs">

              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="aik-tab">

                  <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <strong>Klik gambar untuk memperbesar
                  </div>
                  
                  <div class="mt15">

                  <form id="tambah-gelombang" class="form-horizontal form-label-left">

                    <div class="overflow-form">
                        <?php for ($i = 0; $i < 6; $i++){ 
                        echo '
                          <div class="col-md-4 col-sm-4 col-xs-12 list-berkas">
                            <div class="form-group border">
                              <div class="title-berkas control-label col-md-12 col-sm-12 col-xs-12">Ijazah
                              </div>
                              <div class="input-file col-md-12 col-sm-12 col-xs-12">
                                <div class="img-wrap">
                                  <img data-toggle="modal" data-target=".preview-img" id="preview-img'.$i.'" src="images/ijazah.jpg" />
                                </div>
                                <div class="clearfix"></div>
                              </div>
                              <div class="clearfix"></div>
                            </div>
                          </div>';}?>

                          <!-- modal -->
                          <div class="modal fade preview-img" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                              <div class="modal-content">

                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                  </button>
                                  <h4 class="modal-title" id="myModalLabel2">File Berkas</h4>
                                </div>
                                <div class="modal-body">
                                  <img class="preview-gbr" src="">
                                </div>
                                <div class="modal-footer center ">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>

                        </div>
                      </div>
                    </div>
                    </div>

                  <div class="ln_solid"></div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Validasi
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">Valid</option>
                          <option value="">Tidak Valid</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tanggal Ujian
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="">09/08/2019</option>
                          <option value="">07/09/2019</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Catatan
                      </label>
                      <div class="col-md-5 col-sm-5 col-xs-12">
                        <textarea id="" class="form-control" name="" rows=8></textarea>
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="list_peserta.php" class="btn btn-default" type="button">Batal</a>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                      </div>
                    </div>

                    </form>
                  </div>

                </div>

            </div>
          </div>
          

            
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>

<script type="text/javascript">
  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                imgId = '#preview-'+$(input).attr('id');
                $(imgId).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
          var imgIds = '#preview-'+$(input).attr('id');
          $(imgIds).on("click",function(){
            var Imgs=$(this).attr('src');
            $('.preview-gbr').attr('src',Imgs);
          });
      }

      $('.img-wrap img').on("click",function(){
          var Imgs=$(this).attr('src');
          $('.preview-gbr').attr('src',Imgs);
        });


      $(".input-file input[type='file']").change(function(){
        readURL(this);
      });
</script>