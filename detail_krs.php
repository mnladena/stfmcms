<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kartu Rencana Studi S1</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">KRS - S1</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-9 right">
                <a href="krs_s1.php" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
              </div>  
            </div>

            <div class="mt15">

            <!-- jadwal krs  -->
            <div class="modal fade jadwal-krs" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Jadwal KRS</h4>
                  </div>
                  <div class="modal-body">
                      <form id="" class="form-horizontal">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tanggal Kegiatan
                          </label>
                          <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class='input-group date'>
                                <input type='text' class="form-control"  id='datetimepicker6' value="10/07/2019"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                          </div>
                          <div class="col-md-1 col-sm-1 col-xs-12"><span class="inp-text full center"> - </span></div>
                          <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class='input-group date'>
                                <input type='text' class="form-control" value="" placeholder="HH.MM" />
                            </div>
                          </div>
                        </div>
                      <div class="clearfix"></div>

                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tanggal Kegiatan
                          </label>
                          <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class='input-group date'>
                                <input type='text' class="form-control"  id='datetimepicker7' value="10/07/2019"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                          </div>
                          <div class="col-md-1 col-sm-1 col-xs-12"><span class="inp-text full center"> - </span></div>
                          <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class='input-group date'>
                                <input type='text' class="form-control" value="" placeholder="HH.MM" />
                            </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>

                      <div class="mt15">
                        <table id="" class="datatable table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Kode Mata Kuliah</th>
                              <th>Nama Mata Kuliah</th>
                              <th class="no-sort">SKS</th>
                              <th>Jenis</th>
                              <th class="no-sort">&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php for ($i = 0; $i < 30; $i++){ 
                            $j=$i+1;
                            echo '
                              <tr>
                                <td>'.$j.'</td>
                                <td>MPK-120'.$i.'</td>
                                <td>Aplikasi Sistem Komputer Farmasi</td>
                                <td>2/0</td>
                                <td>Wajib</td>
                                <td>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" class="flat">
                                    </label>
                                  </div>
                                </td>
                              </tr>';
                            }?>
                          </tbody>
                        </table>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semester belum dipilih</div>
                      </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah</button>
                  </div>

                </div>
              </div>
            </div>

            
                    <table id="" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Semester</th>
                          <th>Angkatan</th>
                          <th>Jadwal KRS</th>
                          <th>Status</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>


                      <tbody>

                      <?php for ($i = 0; $i < 6; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>Semester 1</td>
                          <td>14</td>
                          <td>1/10/2019 - 08.00</td>
                          <td>Berlangsung</td>
                          <td>
                            <a href="lihat_krs.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> KRS</a>
                            <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".jadwal-krs"><i class="fa fa-edit"></i> Jadwal</button>
                            <button type="button" class="btn btn-xs btn-success">Bypass</button>
                          </td>
                        </tr>';
                      }?>

                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>