<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kemahasiswaan</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="mahasiswa.php">Mahasiswa</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kemahasiswaan</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <!-- start accordion -->
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Filter</h4>
                          
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <div class="row">
                              <form id="" class="form-horizontal form-label-left">

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">NIM
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Nama
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Kelas
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">11A</option>
                                          <option value="">11B</option>
                                          <option value="">12A</option>
                                          <option value="">15A</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>     

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Proram Studi
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Prodi 1</option>
                                          <option value="">Prodi 2</option>
                                          <option value="">Prodi 3</option>
                                          <option value="">Prodi 4</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Jenis Daftar
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Jenis 1</option>
                                          <option value="">Jenis 2</option>
                                          <option value="">Jenis 3</option>
                                          <option value="">Jenis 4</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Status
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Aktif</option>
                                          <option value="">Tidak Aktif</option>
                                          <option value="">Cuti</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>    
                                  
                                  <div class="clearfix"></div>
                                  <div class="ln_solid"></div>

                                  <div class="form-group">
                                    <div class="col-md-12 col-sm-6 col-xs-12 center">
                                       <button class="btn btn-primary" type="reset">Reset</button>
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                              </form>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      
                    </div>
                    <!-- end of accordion -->
            
            <div class="row">
               <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-9  col-sm-offset-9 right">
                  <a href="" class="btn btn-default"><i class="fa fa-file-pdf-o"></i> Download</a>
                </div>
            </div>

            <div class="mt15">

                <table id="" class="datatable table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Mahasiswa</th>
                      <th>Program Studi</th>
                      <th>Kelas</th>
                      <th class="no-sort">Jenis Daftar</th>
                      <th class="no-sort">Status</th>
                      <th class="no-sort">Action</th>
                    </tr>
                  </thead>

                    <tbody>
                      <?php for ($i = 0; $i < 50; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>
                            <div>20190001</div>
                            <div>Saladine Yussie</div>
                          </td>
                          <td>S1 Farmasi</td>
                          <td>
                            14A
                          </td>
                          <td>
                            Reguler
                          </td>
                          <td>
                            Aktif
                          </td>
                          <td>
                            <a href="#" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                          </td>
                        </tr>';
                      }?>
                    </tbody>

                </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>