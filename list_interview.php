<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Soal Interview</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">AIK</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Akademik</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Keuangan</a>
                </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="aik-tab">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".create-aik"><i class="fa fa-plus-circle"></i> Tambah Soal</button>
                  <div class="mt15">
                    <table id="" class="table table-striped table-bordered datatable">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th class="no-sort">Soal AIK</th>
                            <th class="no-sort">Action</th>
                          </tr>
                        </thead>


                        <tbody>

                        <?php for ($i = 0; $i < 30; $i++){ 
                        $j=$i+1;
                        $k=$i+2019;
                        echo '
                          <tr>
                            <td>'.$j.'</td>
                            <td>Apakah yang dimaksud dengan Pathologi?</td>
                            <td>
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".edit-aik"><i class="fa fa-edit"></i> Edit</a>
                              <a href="" class="btn btn-xs btn-danger" data-toggle="modal" data-target=".delete-popup"><i class="fa fa-trash"></i> Delete</a>
                            </td>
                          </tr>';
                        }?>

                        </tbody>
                      </table>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="akademik-tab">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".create-akademik"><i class="fa fa-plus-circle"></i> Tambah Soal</button>
                  <div class="mt15">
                    <table id="" class="table table-striped table-bordered datatable">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Aspek</th>
                            <th class="no-sort">Uraian</th>
                            <th class="no-sort">Action</th>
                          </tr>
                        </thead>


                        <tbody>

                        <?php for ($i = 0; $i < 30; $i++){ 
                        $j=$i+1;
                        $k=$i+2019;
                        echo '
                          <tr>
                            <td>'.$j.'</td>
                            <td>Penampilan Fisik</td>
                            <td>Badan sehat den tegap, pakain bersih dan rapih, wajah penuh semangat</td>
                            <td>
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".edit-akademik"><i class="fa fa-edit"></i> Edit</a>
                              <a href="" class="btn btn-xs btn-danger" data-toggle="modal" data-target=".delete-popup"><i class="fa fa-trash"></i> Delete</a>
                            </td>
                          </tr>';
                        }?>

                        </tbody>
                      </table>
                  </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="keuangan-tab">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".create-keuangan"><i class="fa fa-plus-circle"></i> Tambah Soal</button>
                  <div class="mt15">
                      <table id="" class="table table-striped table-bordered datatable">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Aspek</th>
                              <th class="no-sort">Uraian</th>
                              <th class="no-sort">Action</th>
                            </tr>
                          </thead>


                          <tbody>

                          <?php for ($i = 0; $i < 30; $i++){ 
                          $j=$i+1;
                          $k=$i+2019;
                          echo '
                            <tr>
                              <td>'.$j.'</td>
                              <td>Pembiayaan</td>
                              <td>Pembiayaan untuk calon mahasiswa</td>
                              <td>
                                <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".edit-akademik"><i class="fa fa-edit"></i> Edit</a>
                                <a href="" class="btn btn-xs btn-danger" data-toggle="modal" data-target=".delete-popup"><i class="fa fa-trash"></i> Delete</a>
                              </td>
                            </tr>';
                          }?>

                          </tbody>
                        </table>
                  </div>
                </div>
            </div>
          </div>
          

          <!-- delete -->
          <div class="modal fade delete-popup" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Konfirmasi Hapus Data</h4>
                  </div>
                  <div class="modal-body">
                    Apakah Anda yakin ingin menghapus data?
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Hapus</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- create  -->
            <div class="modal fade create-aik" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Soal AIK</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label" for="">Soal
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5></textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <div class="modal fade create-akademik" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Soal Akademik</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="">Aspek
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Uraian
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5></textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <div class="modal fade create-keuangan" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Soal Keuangan</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="">Aspek
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Uraian
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5></textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit -->
            <div class="modal fade edit-aik" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Soal AIK</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label" for="">Soal
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5>Apakah yang dimaksud dengan Ketergantungan gadget?</textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <div class="modal fade edit-akademik" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Soal Akademik</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="">Aspek
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="Penampilan Fisik">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Uraian
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5>Badan sehat den tegap, pakain bersih dan rapih, wajah penuh semangat</textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <div class="modal fade edit-keuangan" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Soal Keuangan</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="">Aspek
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="Pembiayaan">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Uraian
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5>Pembiayaan untuk calon mahasiswa</textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>