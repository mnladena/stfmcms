<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Profile</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              Profil berhasil disimpan
            </div>

                  <form id="" class="form-horizontal form-label-left">

                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Foto
                      </label>
                      <div class="list-berkas user col-md-4 col-sm-4 col-xs-12">
                        <div class="input-file">
                            <div class="img-wrap">
                              <img id="preview-img1" src="images/img.jpg" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="btn-wrap">
                              <input type="file" name="" id="img1"><label>Ubah</label>
                            </div>
                          </div>
                        </div>
                    </div>

                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">Salahudin Yusuf</div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Gelar Depan
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Gelar Belakang
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">M.Kom</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jenis Kelamin
                      </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="inp-text">Laki-laki</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Agama
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="inp-text">Islam</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Golongan Darah
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="inp-text">AB</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tempat Lahir
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="inp-text">Kupang</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tanggal Lahir
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="inp-text">01/01/1991</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Email
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="inp-text">yusuf@insistdigital.com</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nomor HP
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="inp-text">08219876556</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Alamat</label>
                      <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="inp-text full">Jalan Mawar mekar no 32 RT 01/02</div>
                        <div class="inp-text full">Kota Depok</div>
                        <div class="inp-text full">Kecamatan Depok</div>
                        <div class="inp-text full">Kelurahan Depok</div>
                        <div class="inp-text full">16190</div>
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pendidikan Terakhir
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">S1-Teknik Informatika</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Asal Pendidikan
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">Universitas Bina Nusantara</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NBM
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NIK STFM
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NIDN
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NIK Dosen
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NIK DPK
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jabatan Akademik
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Serdos
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Program Studi
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="inp-text">-</div>
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="pengguna.php" class="btn btn-default">Batal</a>
                        <a href="edit_profil.php" class="btn btn-primary">Edit</a>
                      </div>
                    </div>

                    </form>
                  </div>

        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>

<script type="text/javascript">
  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                imgId = '#preview-'+$(input).attr('id');
                $(imgId).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
          var imgIds = '#preview-'+$(input).attr('id');
          $(imgIds).on("click",function(){
            var Imgs=$(this).attr('src');
            $('.preview-gbr').attr('src',Imgs);
          });
      }

      $('.img-wrap img').on("click",function(){
          var Imgs=$(this).attr('src');
          $('.preview-gbr').attr('src',Imgs);
        });


      $(".input-file input[type='file']").change(function(){
        readURL(this);
      });

</script>