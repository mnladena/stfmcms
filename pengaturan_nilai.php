<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Pengaturan Nilai</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Pengaturan Penilaian</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="" role="tabpanel" data-example-id="togglable-tabs">

              <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="range" role="tab" data-toggle="tab" aria-expanded="true">Range Nilai</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" id="teori" role="tab" data-toggle="tab" aria-expanded="false">Bobot Teori</a>
                <li role="presentation" class=""><a href="#tab_content3" id="praktikum" role="tab" data-toggle="tab" aria-expanded="false">Bobot Praktikum</a>
                </li>
              </ul>

              <div id="myTabContent" class="tab-content">

                  <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="range-tab">

                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-range"><i class="fa fa-plus-circle"></i> Tambah Range Nilai</button>

                      <div class="mt15">
                        <table id="" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>A</th>
                            <th>B</th>
                            <th>C</th>
                            <th>D</th>
                            <th>E</th>
                            <th>Status</th>
                            <th class="no-sort">Action</th>
                          </tr>
                        </thead>


                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>80-100</td>
                            <td>68-79</td>
                            <td>56-67</td>
                            <td>45-55</td>
                            <td>0-44</td>
                            <td><span class="text-success">Aktif</span></td>
                            <td>
                              <button data-toggle="modal" data-target=".edit-range" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                            </td>
                          </tr>

                          <tr>
                            <td>2</td>
                            <td>80-100</td>
                            <td>68-79</td>
                            <td>56-67</td>
                            <td>45-55</td>
                            <td>0-44</td>
                            <td><span class="text-off">Tidak Aktif</span></td>
                            <td>
                              <button data-toggle="modal" data-target=".edit-range" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </div>

                  </div>

                  <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="teori-tab">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-teori"><i class="fa fa-plus-circle"></i> Tambah Bobot Teori</button>

                      <div class="mt15">
                        <table id="" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Absensi</th>
                            <th>Etika & Tugas</th>
                            <th>UTS</th>
                            <th>UAS</th>
                            <th>Status</th>
                            <th class="no-sort">Action</th>
                          </tr>
                        </thead>


                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>10%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td><span class="text-success">Aktif</span></td>
                            <td>
                              <button data-toggle="modal" data-target=".edit-teori" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                            </td>
                          </tr>

                          <tr>
                            <td>2</td>
                            <td>10%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td><span class="text-off">Tidak Aktif</span></td>
                            <td>
                              <button data-toggle="modal" data-target=".edit-teori" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </div>

                  </div>

                  <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="praktikum-tab">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-praktikum"><i class="fa fa-plus-circle"></i> Tambah Bobot Praktikum</button>

                      <div class="mt15">
                        <table id="" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Absensi</th>
                            <th>Etika & Tugas</th>
                            <th>Pre/Post Test</th>
                            <th>Laporan</th>
                            <th>UAP</th>
                            <th>Status</th>
                            <th class="no-sort">Action</th>
                          </tr>
                        </thead>


                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>10%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td><span class="text-success">Aktif</span></td>
                            <td>
                              <button data-toggle="modal" data-target=".edit-praktikum" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                            </td>
                          </tr>

                          <tr>
                            <td>2</td>
                            <td>10%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td>30%</td>
                            <td><span class="text-off">Tidak Aktif</span></td>
                            <td>
                              <button data-toggle="modal" data-target=".edit-praktikum" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                            </td>
                          </tr>

                        </tbody>
                      </table>
                    </div>

                  </div>

              </div>

            </div>

            <!-- create  -->
            <div class="modal fade tambah-range" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Range Nilai</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                        <table class="table_info table_info-medium table_info-center table-striped">
                          <tr>
                            <td align="center">A</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                          </tr>
                          <tr>
                            <td align="center">B</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                          </tr>
                          <tr>
                            <td align="center">C</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                          </tr>
                          <tr>
                            <td align="center">D</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                          </tr>
                          <tr>
                            <td align="center">E</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                          </tr>
                        </table>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit  -->
            <div class="modal fade edit-range" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Range Nilai</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                        <table class="table_info table_info-medium table_info-center table-striped">
                          <tr>
                            <td align="center">A</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="80" disabled>
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="" value="80" disabled>
                            </td>
                          </tr>
                          <tr>
                            <td align="center">B</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="68" disabled>
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="" value="79" disabled>
                            </td>
                          </tr>
                          <tr>
                            <td align="center">C</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="56" disabled>
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="" value="67" disabled>
                            </td>
                          </tr>
                          <tr>
                            <td align="center">D</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="45" disabled>
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="" value="45" disabled>
                            </td>
                          </tr>
                          <tr>
                            <td align="center">E</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="0" disabled>
                            </td>
                            <td width="20" align="center">-</td>
                            <td width="80">
                              <input type="text" name="" value="44" disabled>
                            </td>
                          </tr>
                          <tr>
                            <td align="center">Status</td>
                            <td width="20">:</td>
                            <td colspan="3">
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox" class="flat"> Aktif
                                </label>
                              </div>
                            </td>
                          </tr>
                        </table>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            
            <!-- create bobot teori -->
            <div class="modal fade tambah-teori" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Bobot Teori</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                        <table class="table_info table_info-medium table_info-center table-striped">
                          <tr>
                            <td align="center">Absensi</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Tugas & Etika</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">UTS</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">UAS</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                        </table>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit bobot teori  -->
            <div class="modal fade edit-teori" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Bobot Teori</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                        <table class="table_info table_info-medium table_info-center table-striped">
                          <tr>
                            <td align="center">Absensi</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Tugas & Etika</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">UTS</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">UAS</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Status</td>
                            <td width="20">:</td>
                            <td colspan="2">
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox" class="flat"> Aktif
                                </label>
                              </div>
                            </td>
                          </tr>
                        </table>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>


             <!-- create bobot praktikum -->
            <div class="modal fade tambah-praktikum" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Bobot Praktikum</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                        <table class="table_info table_info-medium table_info-center table-striped">
                          <tr>
                            <td align="center">Absensi</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Etika & Keaktifan</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Pre/Post Test</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Laporan</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">UAP</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="">
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                        </table>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit bobot praktikum  -->
            <div class="modal fade edit-praktikum" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Bobot Praktikum</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                        <table class="table_info table_info-medium table_info-center table-striped">
                          <tr>
                            <td align="center">Absensi</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Etika & Keaktifan</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Pre/Post Test</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Laporan</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">UAP</td>
                            <td width="20">:</td>
                            <td width="80">
                              <input type="text" name="" value="10" disabled>
                            </td>
                            <td width="20" align="center">%</td>
                          </tr>
                          <tr>
                            <td align="center">Status</td>
                            <td width="20">:</td>
                            <td colspan="2">
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox" class="flat"> Aktif
                                </label>
                              </div>
                            </td>
                          </tr>
                        </table>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

                    
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>