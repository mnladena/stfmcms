<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kelas</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="mahasiswa.php">Mahasiswa</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kelas</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>
            
            <div class="soal-desc">
              <table class="table_info-noborder">
                <tr>
                  <td><b>Nama Kelas</b></td>
                  <td width="50">:</td>
                  <td>14S1A</td>
                </tr>
                <tr>
                  <td><b>Pembimbing Akademik</b></td>
                  <td width="50">:</td>
                  <td>Abdul Azis Setiawan</td>
                </tr>
              </table>
            </div>

            <div class="row">
               <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-9  col-sm-offset-9 right">
                  <a href="" class="btn btn-default"><i class="fa fa-file-pdf-o"></i> Download</a>
                </div>
            </div>

            <div class="mt15">

                <table id="" class="datatable table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NIM</th>
                      <th>Nama Mahasiswa</th>
                      <th class="no-sort">Status</th>
                      <th class="no-sort">Action</th>
                    </tr>
                  </thead>

                    <tbody>
                      <?php for ($i = 0; $i < 50; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>
                             20190001
                          </td>
                          <td>
                            Saladine Yussie
                          </td>
                          <td>
                            Aktif
                          </td>
                          <td>
                            <a href="#" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                          </td>
                        </tr>';
                      }?>
                    </tbody>

                </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>