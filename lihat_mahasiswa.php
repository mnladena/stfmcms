<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kartu Rencana Studi S1</h3>
                <h4>Mata Kuliah Semester 5 - Obat Bahan Alami -  Kelas A</h4>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">KRS - S1</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            
            <div class="row">
              <div class="col-md-9 col-sm-9 col-xs-9">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-mhs"><i class="fa fa-plus-circle"></i> Tambah Mahasiswa</button>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 right">
                <a href="lihat_kelas.php" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
              </div>
            </div>

            <div class="mt15">

            <!-- Tambah mhs  -->
            <div class="modal fade tambah-mhs" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Mahasiswa</h4>
                  </div>
                  <div class="modal-body">
                      <form id="" class="form-horizontal">

                        <table id="" class="datatable table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Mahasiswa</th>
                              <th>NIM</th>
                              <th class="no-sort">&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php for ($i = 0; $i < 30; $i++){ 
                            $j=$i+1;
                            echo '
                              <tr>
                                <td>'.$j.'</td>
                                <td>Saladina Yussie</td>
                                <td>20190001</td>
                                <td>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" class="flat">
                                    </label>
                                  </div>
                                </td>
                              </tr>';
                            }?>
                          </tbody>
                        </table>

                      <!-- <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Mahasiswa belum dipilih</div> -->

                      </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- delete -->
          <div class="modal fade hapus-mhs" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Hapus Kelas</h4>
                  </div>
                  <div class="modal-body text-center">
                    <div>Apakah Anda yakin ingin menghapus mahasiswa?</div> 
                    <div><b>Saladina Yussie - 20190001</b></div>

                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Hapus</button>
                  </div>

                </div>
              </div>
            </div>

            
                    <table id="" class="datatable table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Mahasiswa</th>
                          <th>NIM</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>


                      <tbody>

                      <?php for ($i = 0; $i < 55; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>Saladina Yussie</td>
                          <td>20190001</td>
                          <td>
                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target=".hapus-mhs"><i class="fa fa-trash"></i> Hapus</button>
                          </td>
                        </tr>';
                      }?>

                      </tbody>
                    </table>

                               
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>