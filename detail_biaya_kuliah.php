<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Pembiayaan Perkuliahan</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <div class="" role="tabpanel" data-example-id="togglable-tabs">

              <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">D3 Farmasi</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">S1 Farmasi</a>
                </li>
              </ul>

              <div id="myTabContent" class="tab-content">

                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="d3-tab">
                  
                  <div class="mt15">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-versi"><i class="fa fa-plus-circle"></i> Tambah Versi</button>

                    <div class="mt15">
                      <table id="" class="table table-striped table-bordered datatable">
                        <thead>
                          <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Tahun</th>
                            <th class="no-sort" rowspan="2">BOP</th>
                            <th colspan="4">BPP</th>
                            <th class="no-sort" rowspan="2">Action</th>
                          </tr>
                          <tr>
                            <td class="no-sort">Gelombang 1</td>
                            <td class="no-sort">Gelombang 2</td>
                            <td class="no-sort">Gelombang 3</td>
                            <td class="no-sort">Gelombang 4</td>
                          </tr>
                        </thead>


                        <tbody>
                        <?php for ($i = 0; $i < 30; $i++){ 
                        $j=$i+1;
                        $k=$i+2019;
                        echo '
                          <tr>
                            <td>
                              '.$j.'
                            </td>
                            <td>
                              2019
                            </td>
                            <td>
                              Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                              -
                            </td>
                            <td>
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".edit-versi"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                          </tr>';
                        }?>
                        </tbody>
                      </table>
                    </div>
                    

                  
                  </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="s1-tab">
                  
                    <div class="mt15">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-versi"><i class="fa fa-plus-circle"></i> Tambah Versi</button>

                    <div class="mt15">
                      <table id="" class="table table-striped table-bordered datatable">
                        <thead>
                          <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Tahun</th>
                            <th class="no-sort" rowspan="2">BOP</th>
                            <th colspan="4">BPP</th>
                            <th class="no-sort" rowspan="2">Status</th>
                            <th class="no-sort" rowspan="2">Action</th>
                          </tr>
                          <tr>
                            <td class="no-sort">Gelombang 1</td>
                            <td class="no-sort">Gelombang 2</td>
                            <td class="no-sort">Gelombang 3</td>
                            <td class="no-sort">Gelombang 4</td>
                          </tr>
                        </thead>


                        <tbody>
                        <?php for ($i = 0; $i < 30; $i++){ 
                        $j=$i+1;
                        $k=$i+2019;
                        echo '
                          <tr>
                            <td>
                              '.$j.'
                            </td>
                            <td>
                              2019
                            </td>
                            <td>
                              Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                               Rp 1.500.000
                            </td>
                            <td>
                              -
                            </td>
                            <td>
                              Aktif
                            </td>
                            <td>
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".edit-versi"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                          </tr>';
                        }?>
                        </tbody>
                      </table>
                    </div>
                    

                  
                  </div>

                </div>

                
            </div>

          </div>

          <!-- tambah versi -->
            <div class="modal fade tambah-versi" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Biaya</h4>
                  </div>
                  <div class="modal-body">
                    <form id="demo-form2" class="form-horizontal form-label-left box">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Versi
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" disabled class="form-control col-md-7 col-xs-12" value="Versi 2">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">BOP(Perkuliahan)
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                        </div>
                      </div>

                      <div class="title-label col-md-12 col-sm-12 col-xs-12">BPP (Bangunan)</div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Gelombang 1
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                        </div>
                        <div class='btn-wrap'>
                            <div class="btn btn-add"><i class="fa fa-plus-circle"></i></div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Gelombang 2
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                        </div>
                        <div class='btn-wrap'>
                            <div class="btn btn-add"><i class="fa fa-plus-circle"></i></div>
                            <div class="btn btn-del"><i class="fa fa-minus-circle"></i></div>
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input required="required" type="radio" class="flat left" checked value="jenjang1" id="jenjang1" name="jenjang"> <span class="inp-text left">Aktif</span>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input required="required" type="radio" class="flat left" value="jenjang2" id="jenjang2" name="jenjang"> <span class="inp-text left">Tidak Aktif</span>
                        </div>
                      </div>
                    </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit -->
            <div class="modal fade edit-versi" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Versi</h4>
                  </div>
                  <div class="modal-body">
                    <form id="demo-form2" class="form-horizontal form-label-left box">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Versi
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" class="form-control col-md-7 col-xs-12" value="Versi 2">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">BOP(Perkuliahan)
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" disabled class="form-control col-md-7 col-xs-12" value="1.500.000">
                        </div>
                      </div>

                      <div class="title-label col-md-12 col-sm-12 col-xs-12">BPP (Bangunan)</div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Gelombang 1
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" disabled class="form-control col-md-7 col-xs-12" value="9.500.000">
                        </div>
                        <div class='btn-wrap'>
                            <div class="btn btn-add"><i class="fa fa-plus-circle"></i></div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Gelombang 2
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" disabled class="form-control col-md-7 col-xs-12" value="10.000.000">
                        </div>
                        <div class='btn-wrap'>
                            <div class="btn btn-add"><i class="fa fa-plus-circle"></i></div>
                            <div class="btn btn-del"><i class="fa fa-minus-circle"></i></div>
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input required="required" type="radio" class="flat left" checked value="jenjang1" id="jenjang1" name="jenjang"> <span class="inp-text left">Aktif</span>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input required="required" type="radio" class="flat left" value="jenjang2" id="jenjang2" name="jenjang"> <span class="inp-text left">Tidak Aktif</span>
                        </div>
                      </div>
                    </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>
          

            
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>