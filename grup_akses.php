<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Data Grup Akses</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Grup Akses</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              Grup Akses Berhasil Ditambahkan
            </div>
            
            <a href="grup_akses-tambah.php" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah Grup Akses</a>

            

            <!-- hapus hak akses  -->
            <div class="modal fade hapus-akses" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Hapus Hak Akses</h4>
                  </div>

                  <div class="modal-body center">
                    <p>
                      Apakah Anda yakin ingin menghapus hak akses
                      <div>
                        <strong>"Keuangan"</strong>
                      </div>
                    </p>
                  </div>

                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Hapus</button>
                  </div>

                </div>
              </div>
            </div>

            <div class="mt15">

                <table id="" class="datatable table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Grup Akses</th>
                      <th>Deskripsi</th>
                      <th class="no-sort">Action</th>
                    </tr>
                  </thead>

                    <tbody>
                      <?php for ($i = 0; $i < 20; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>
                            Keuangan
                          </td>
                          <td>
                            Akses yang diberikan untuk seluruh bagian keuangan
                          </td>
                          <td>
                            <a href="grup_akses-edit.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            <button data-toggle="modal" data-target=".hapus-akses" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                          </td>
                        </tr>';
                      }?>
                    </tbody>

                </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>

  $('#select-akses,#select-akses-edit').selectize({
    plugins: ['remove_button']
});

  $(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>