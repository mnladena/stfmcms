<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kegiatan PMB</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kegiatan PMB</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus-circle"></i> Tambah Tahun</button>

            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Tahun</h4>
                  </div>
                  <div class="modal-body">
                      <label for="heard">Tahun:</label>
                      <select id="heard" class="form-control" required>
                        <option value="">Choose..</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                      </select>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah</button>
                  </div>

                </div>
              </div>
            </div>

            <div class="row">

            <?php for ($i = 0; $i < 9; $i++){ 
              $j=$i+19;
               echo '
              <div class="col-md-4 col-sm-4 col-xs-12">
                <a href="pmb_gelombang.php">
                  <span class="list-tahun">
                    <span class="tahun-kegiatan">20'.$j.'</span>
                    <span class="col-md-6 col-sm-6 kolom">
                      <span class="kolom-head">Pendaftar</span>
                      <span class="kolom-jumlah">320</span>
                    </span>
                    <span class="col-md-6 col-sm-6 kolom lulus">
                      <span class="kolom-head">Lulus</span>
                      <span class="kolom-jumlah">120</span>
                    </span>
                  </span>
                </a>
              </div>
              ';

             } ?>

            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>
