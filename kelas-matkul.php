<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Perkuliahan</h3>
                <h4>Program Studi S1 Farmasi</h4>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="perkuliahan.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Perkuliahan</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-9 right">
                <a href="perkuliahan-kelas.php" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
              </div>  
            </div>

            <div class="mt15">

                <table id="" class="datatable table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Mata Kuliah</th>
                      <th>Waktu</th>
                      <th>Ruang</th>
                      <th>Dosen</th>
                      <th class="no-sort">Action</th>
                    </tr>
                  </thead>

                    <tbody>
                      
                        <tr>
                          <td>
                            1 
                          </td>
                          <td>
                            <div>MPK-5206 - 2/0 SKS</div>
                            <div>AIK5 (Islam dan Ilmu Pengetahuan)</div>
                            <div>Mata Kuliah Wajib</div>
                          </td>
                          <td>
                            <div>Senin</div>
                            <div>09.00 - 10.30</div>
                          </td>
                          <td>
                            <div>Gedung Utama</div>
                            <div>B504</div>
                          </td>
                          <td>
                            Wiro Hartono
                          </td>
                          <td>
                            <a href="edit-matkul.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                          </td>
                        </tr>

                        <tr>
                          <td>
                            2 
                          </td>
                          <td>
                            <div>MPK-5206 - 2/0 SKS</div>
                            <div>AIK5 (Islam dan Ilmu Pengetahuan)</div>
                            <div>Mata Kuliah Tambahan</div>
                          </td>
                          <td>
                            <div>Senin</div>
                            <div>09.00 - 10.30</div>
                          </td>
                          <td>
                            <div>Gedung Utama</div>
                            <div>B504</div>
                          </td>
                          <td>
                            Wiro Hartono
                          </td>
                          <td>
                            <a href="edit-matkul.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                          </td>
                        </tr>

                        <tr>
                          <td>
                            3
                          </td>
                          <td>
                            <div>MPK-5206 - 2/0 SKS</div>
                            <div>Magang</div>
                            <div>Mata Kuliah Tambahan</div>
                          </td>
                          <td>
                            
                          </td>
                          <td>
                            
                          </td>
                          <td>
                            
                          </td>
                          <td>
                            
                          </td>
                        </tr>

                    </tbody>

                </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>