<?php include "include/head.php" ?>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <div class="site_logo center"><img src="images/logo.png" alt=""></div>
          <section class="login_content">
            <form>
              <h1>Login</h1>
              <div>
                <input type="text" class="form-control" placeholder="NID" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.php">Log in</a>
                <a class="reset_pass" href="#">Lupa password?</a>
              </div>

              <div class="clearfix"></div>

            </form>
          </section>
        </div>

      </div>
    </div>
  </body>
</html>
