<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Tambah Grup Akses</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Grup Akses</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>
            
            <div class="soal-desc">
              <form id="" class="form-horizontal form-label-left">

                    <div class="col-md-8 col-sm-8 col-xs-12">


                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Nama Grup Akses
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Deskripsi
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="" name="" rows="5" class="form-control col-md-7 col-xs-12"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Akses Data Mahasiswa
                          </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <div class="radio pilihan-jawaban">
                                <input type="radio" class="flat left" value="jenis1" id="jenis1" name="jenis"> <span class="inp-text left">Farmasi S1</span>
                          </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <div class="radio pilihan-jawaban">
                            <input type="radio" class="flat left" value="jenis2" id="jenis2" name="jenis"> <span class="inp-text left">Farmasi D3</span>
                          </div>
                        </div>
                      </div>

                    </div>     
                
            </div>

            <div class="mt15">

              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">PMB</a>
                  </li>
                  <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Mahasiswa</a>
                  </li>
                  <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Akademik</a>
                  </li>
                  <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Keuangan</a>
                  </li>
                  <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Pengelola Pengguna</a>
                  </li>
                </ul>

                <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Menu</th>
                          <th class="text-center">Lihat</th>
                          <th class="text-center">Tambah</th>
                          <th class="text-center">Edit</th>
                          <th class="text-center">Hapus</th>
                          <th class="text-center">Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Kegiatan PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Gelombang PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Calon PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Absensi Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Proses Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Menu</th>
                          <th class="text-center">Lihat</th>
                          <th class="text-center">Tambah</th>
                          <th class="text-center">Edit</th>
                          <th class="text-center">Hapus</th>
                          <th class="text-center">Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Kegiatan PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Gelombang PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Calon PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Absensi Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Proses Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Menu</th>
                          <th class="text-center">Lihat</th>
                          <th class="text-center">Tambah</th>
                          <th class="text-center">Edit</th>
                          <th class="text-center">Hapus</th>
                          <th class="text-center">Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Kegiatan PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Gelombang PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Calon PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Absensi Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Proses Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                   <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Menu</th>
                          <th class="text-center">Lihat</th>
                          <th class="text-center">Tambah</th>
                          <th class="text-center">Edit</th>
                          <th class="text-center">Hapus</th>
                          <th class="text-center">Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Kegiatan PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Gelombang PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Calon PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Absensi Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Proses Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Menu</th>
                          <th class="text-center">Lihat</th>
                          <th class="text-center">Tambah</th>
                          <th class="text-center">Edit</th>
                          <th class="text-center">Hapus</th>
                          <th class="text-center">Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Kegiatan PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Gelombang PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Calon PMB</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td>
                            <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Absensi Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Proses Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Soal Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Ujian</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Report Interview</td>
                          <td>
                             <div class="checkbox pilihan-jawaban text-center">
                                <input type="checkbox" class="flat left" value="" id="" name="">
                            </div>
                          </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="ln_solid"></div>

              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                  <a href="grup_akses.php" class="btn btn-default">Batal</a>
                  <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
              </div>
              
              </form>
                
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>