<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kegiatan PMB</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kegiatan PMB</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="mt15">

            <form id="tambah-gelombang" class="form-horizontal form-label-left">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gelombang
                </label>
                <div class="col-md-1 col-sm-1 col-xs-12">
                  <input type="text" id="" disabled class="form-control col-md-7 col-xs-12" value="2">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tanggal Kegiatan
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class='input-group date'>
                      <input type='text' class="form-control"  id='datetimepicker6'/>
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
                <div class="col-md-1 col-sm-1 col-xs-12"><span class="inp-text full center">s/d</span></div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class='input-group date'>
                      <input type='text' class="form-control"  id='datetimepicker7'/>
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tanggal Ujian
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class='input-group date'>
                        <input type='text' class="form-control datepicker" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class='btn-wrap'>
                    <div class="btn btn-add"><i class="fa fa-plus-circle"></i></div>
                    <div class="btn btn-del"><i class="fa fa-minus-circle"></i></div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tanggal Pengumuman Kelulusan
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class='input-group date'>
                        <input type='text' class="form-control datepicker" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tanggal Pembayaran BPP/BOP
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class='input-group date'>
                        <input type='text' class="form-control datepicker" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Biaya-biaya
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th></th>
                          <th>S1</th>
                          <th>D3</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Biaya Registrasi</td>
                          <td colspan="2" align="center">Rp 250.000</td>
                        </tr>
                        <tr>
                          <td>BPP</td>
                          <td>Rp 6.000.000</td>
                          <td>Rp 4.000.000</td>
                        </tr>
                        <tr>
                          <td>Administrasi</td>
                          <td>Rp 1.100.000</td>
                          <td>Rp 1.000.000</td>
                        </tr>
                        <tr>
                          <td>BOP</td>
                          <td>Rp 14.000.000</td>
                          <td>Rp 12.000.000</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Biaya-biaya
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="jumbotron">
                      <p>Biaya Tidak Tersedia</p>
                    </div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Form Kelulusan
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div id="upload">
                      <input name="file" type="file" multiple />
                    </div>
                    <div class="small-text">*File dalam bentuk PDF</div>
                </div>
              </div>
              
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn" type="button">Cancel</button>
                  <button class="btn btn-primary" type="reset">Reset</button>
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>

            </form>

            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        //upload
        const inputElement = document.querySelector('input[type="file"]');
        const pond = FilePond.create( inputElement );
        FilePond.setOptions({
            server: 'http://127.0.0.1'
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>