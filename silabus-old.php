<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Pengaturan Mata Kuliah - Silabus</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="pengaturan_matkul.php">Pengaturan</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Silabus</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <table class="table_info table_info-medium table_info-center">
            <tr>
              <td width="40%" align="right">Kode Mata Kuliah</td>
              <td width="2%">:</td>
              <td width="40%">MKK-13055</td>
            </tr>
            <tr>
              <td align="right">Nama Mata Kuliah</td>
              <td>:</td>
              <td>Fisika Dasar</td>
            </tr>
            <tr>
              <td align="right">Kategori</td>
              <td>:</td>
              <td>Wajib</td>
            </tr>
            <tr>
              <td align="right">Jenis Mata Kuliah</td>
              <td>:</td>
              <td>Teori & Praktek</td>
            </tr>
            <tr>
              <td align="right">SKS Teori</td>
              <td>:</td>
              <td>2</td>
            </tr>
            <tr>
              <td align="right">SKS Praktek</td>
              <td>:</td>
              <td>1</td>
            </tr>
          </table>
          <div class="clearfix"></div>

          <div class="mt15">

            <div class="" role="tabpanel" data-example-id="togglable-tabs">

              <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Silabus Teori</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" id="berkas-tab" role="tab" data-toggle="tab" aria-expanded="false">Silabus Praktek</a>
                </li>
              </ul>

              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="aik-tab">
                  
                  <div class="mt15">

                  <form id=" class="form-horizontal form-label-left">

                  <div class="mt15">

                    <h3 class="title_bar">Perencanaan Pembelajaran</h3>

                    <div class="form-group">
                      <label class="col-md-12 col-sm-12 col-xs-12">Deskripsi</label>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <textarea class="form-control" rows="5" placeholder=""></textarea>
                      </div>
                      <div class="clearfix"></div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-12 col-sm-12 col-xs-12">Tujuan Pembelajaran</label>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <textarea class="form-control" rows="5" placeholder=""></textarea>
                      </div>
                      <div class="clearfix"></div>
                    </div>

                    </div>  
                    <div class="clearfix"></div>


                    <div class="mt15">

                      <h3 class="title_bar ml10">Pelaksanaan Kegiatan</h3>

                      <div class="sub-title_bar ml10">Jadwal Kegiatan Mingguan</div>

                      <button type="button" class="ml10 btn btn-primary" data-toggle="modal" data-target=".tambah-matkul"><i class="fa fa-plus-circle"></i> Tambah Jadwal</button>


                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Pertemuan</th>
                            <th>Topik</th>
                            <th>Substansi</th>
                            <th>Metode</th>
                            <th>Fasilitas</th>
                            <th>&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <input type="text" name="">
                            </td>
                            <td>
                              <textarea class="resizable_textarea form-control" placeholder=""></textarea>
                            </td>
                            <td>
                              <textarea class="resizable_textarea form-control" placeholder=""></textarea>
                            </td>
                            <td>
                              <textarea class="resizable_textarea form-control" placeholder=""></textarea>
                            </td>
                            <td>
                              <textarea class="resizable_textarea form-control" placeholder=""></textarea>
                            </td>
                            <td>
                              <div class="btn-wrap">
                                <div class="btn btn-del"><i class="fa fa-minus-circle"></i></div>
                            </div>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <input type="text" name="">
                            </td>
                            <td>
                              <textarea class="resizable_textarea form-control" placeholder=""></textarea>
                            </td>
                            <td>
                              <textarea class="resizable_textarea form-control" placeholder=""></textarea>
                            </td>
                            <td>
                              <textarea class="resizable_textarea form-control" placeholder=""></textarea>
                            </td>
                            <td>
                              <textarea class="resizable_textarea form-control" placeholder=""></textarea>
                            </td>
                            <td>
                              <div class="btn-wrap">
                                <div class="btn btn-del"><i class="fa fa-minus-circle"></i></div>
                            </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>

                    </div>

                    <div class="mt15">

                      <h3 class="title_bar">Perencanaan Evaluasi Pembelajaran</h3>

                      <div class="form-group">
                        <label class="col-md-12 col-sm-12 col-xs-12">Hasil Pembelajaran</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control" rows="5" placeholder=""></textarea>
                        </div>
                        <div class="clearfix"></div>
                      </div>

                      <div class="sub-title_bar ml10">Penilaian</div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <table class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th class="center" colspan="2">Konversi Nilai</th>
                                </tr>
                              </thead>
                              <tbody>
                              <tr>
                                <td>A</td>
                                <td>80-100</td>
                              </tr>
                              <tr>
                                <td>B</td>
                                <td>68-79</td>
                              </tr>
                              <tr>
                                <td>C</td>
                                <td>56-67</td>
                              </tr><tr>
                                <td>D</td>
                                <td>45-55</td>
                              </tr>
                              <tr>
                                <td>E</td>
                                <td>0-44</td>
                              </tr>
                              
                              </tbody>
                            </table>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <table class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th class="center" colspan="2">Bobot Penilaian</th>
                                </tr>
                              </thead>
                              <tbody>
                              <tr>
                                <td>Absensi</td>
                                <td>10%</td>
                              </tr>
                              <tr>
                                <td>Tugas</td>
                                <td>30%</td>
                              </tr>
                              <tr>
                                <td>UTS</td>
                                <td>30%</td>
                              </tr><tr>
                                <td>UAS</td>
                                <td>30%</td>
                              </tr>
                              
                              </tbody>
                            </table>

                        </div>

                      <div class="form-group">
                      <label class="col-md-12 col-sm-12 col-xs-12">Daftar Pustaka</label>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <textarea class="form-control" rows="5" placeholder=""></textarea>
                      </div>
                      <div class="clearfix"></div>
                    </div>

                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group center">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target=".submit-interview">Simpan</button>
                      </div>
                    </div>

                    </form>
                  </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="berkas-tab">
                  
                  <div class="mt15">

                    <div class="clearfix"></div>
                    
                  </div>

                </div>

            </div>
          </div>
          

            
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>

<script type="text/javascript">
  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                imgId = '#preview-'+$(input).attr('id');
                $(imgId).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
          var imgIds = '#preview-'+$(input).attr('id');
          $(imgIds).on("click",function(){
            var Imgs=$(this).attr('src');
            $('.preview-gbr').attr('src',Imgs);
          });
      }

      $('.img-wrap img').on("click",function(){
          var Imgs=$(this).attr('src');
          $('.preview-gbr').attr('src',Imgs);
        });


      $(".input-file input[type='file']").change(function(){
        readURL(this);
      });

</script>