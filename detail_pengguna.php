<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Detail Pengguna</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Pengaturan</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <div class="" role="tabpanel" data-example-id="togglable-tabs">

              <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Biodata Pribadi</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" id="" role="tab" data-toggle="tab" aria-expanded="false">Biodata Akademik</a>
                </li>
              </ul>

              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="aik-tab">
                  
                  <div class="mt15">

                  <form id="" class="form-horizontal form-label-left">

                  <h3>Dosen</h3>


                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Foto
                      </label>
                      <div class="list-berkas user col-md-4 col-sm-4 col-xs-12">
                        <div class="input-file">
                            <div class="img-wrap">
                              <img id="preview-img1" src="" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="btn-wrap">
                              <input type="file" name="" id="img1"><label>Browse</label>
                            </div>
                          </div>
                        </div>
                    </div>

                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama*
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-7 col-xs-12" value="Salahudin Yusuf">
                        <span class="text-default small">* Tanpa gelar depan dan belakang</span>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gelar Depan
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-4 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gelar Belakang
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-5 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Kelamin
                      </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input type="radio" class="flat left" value="jenis1" id="jenis1" name="jenis"> <span class="inp-text left">Laki-laki</span>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input type="radio" class="flat left" value="jenis2" id="jenis2" name="jenis"> <span class="inp-text left">Perempuan</span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Agama
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select id="" class="form-control">
                          <option value="">Choose..</option>
                          <option value="">Islam</option>
                          <option value="">Kristen</option>
                          <option value="">Buddha</option>
                          <option value="">Hindu</option>
                          <option value="">Kepercayaan</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Golongan Darah
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select id="" class="form-control">
                          <option value="">Choose..</option>
                          <option value="">AB</option>
                          <option value="">B</option>
                          <option value="">O</option>
                          <option value="">AB</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tempat/Tanggal Lahir
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class='input-group date'>
                            <input type='text' class="form-control"  id='datetimepicker7' value=""/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor HP
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat                       </label>
                      <div class="col-md-5 col-sm-5 col-xs-12">
                        <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Kota Bogor</option>
                            <option value="">Kabupaten Bogor</option>
                            <option value="">Jakarta</option>
                            <option value="">Tangerang</option>
                            <option value="">Bekasi</option>
                          </select>
                        </div>
                    </div>
                        
                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Kecamatan Bogor Timur</option>
                            <option value="">Kabupaten Bogor</option>
                            <option value="">Jakarta</option>
                            <option value="">Tangerang</option>
                            <option value="">Bekasi</option>
                          </select>
                        </div>
                      </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Kelurahan Loji</option>
                            <option value="">Kabupaten Bogor</option>
                            <option value="">Jakarta</option>
                            <option value="">Tangerang</option>
                            <option value="">Bekasi</option>
                          </select>
                        </div>
                      </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="pengguna.php" class="btn btn-default" type="button">Batal</a>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                        <a href="#tab_content2" id="" role="tab" data-toggle="tab" class="btn btn-warning" aria-expanded="true">Simpan dan form selanjutnya</a>
                      </div>
                    </div>

                    </form>
                  </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="berkas-tab">

                  <form id="" class="form-horizontal form-label-left">

                  <h3>Dosen</h3>

                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pendidikan Terakhir
                      </label>
                      <div class="col-md-1 col-sm-1 col-xs-12">
                        <input type="text" id="" class="form-control" value="">
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-4 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Asal Pendidikan
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-4 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NBM
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-4 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NIK STFM
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-4 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NIDN
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-4 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NIK Dosen
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-4 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">NIK DPK
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="" class="form-control col-md-4 col-xs-12" value="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jabatan Akademik
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select id="" class="form-control">
                          <option value="">Choose..</option>
                          <option value="">Jabatan 1</option>
                          <option value="">Jabatan 2</option>
                          <option value="">Jabatan 3</option>
                          <option value="">Jabatan 4</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Serdos
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" class="flat" value=""> Sudah
                            </label>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Program Studi
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select id="" class="form-control">
                          <option value="">Choose..</option>
                          <option value="">Prodi 1</option>
                          <option value="">Prodi 2</option>
                          <option value="">Prodi 3</option>
                          <option value="">Prodi 4</option>
                        </select>
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="pengguna.php" class="btn btn-default" type="button">Kembali</a>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                      </div>
                    </div>


                  </form>


                </div>

            </div>
          </div>
          

            
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>

<script type="text/javascript">
  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                imgId = '#preview-'+$(input).attr('id');
                $(imgId).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
          var imgIds = '#preview-'+$(input).attr('id');
          $(imgIds).on("click",function(){
            var Imgs=$(this).attr('src');
            $('.preview-gbr').attr('src',Imgs);
          });
      }

      $('.img-wrap img').on("click",function(){
          var Imgs=$(this).attr('src');
          $('.preview-gbr').attr('src',Imgs);
        });


      $(".input-file input[type='file']").change(function(){
        readURL(this);
      });

</script>