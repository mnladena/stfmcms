<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Absensi Ujian</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="">

              <!-- start accordion -->
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Filter</h4>
                          
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <div class="row">
                              <form id="" class="form-horizontal form-label-left">

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">NIRM
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">NAMA
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Jenis Kelamin
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Laki-laki</option>
                                          <option value="">Perempuan</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Status Data
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Status Pendaftaran
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>     

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                  <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Jenjang
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Jenis Daftar
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Status Bayar Daftar
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Status Bayar Kuliah
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Status 1</option>
                                          <option value="">Status 2</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>    
                                  
                                  <div class="clearfix"></div>
                                  <div class="ln_solid"></div>

                                  <div class="form-group">
                                    <div class="col-md-12 col-sm-6 col-xs-12 center">
                                       <button class="btn btn-primary" type="reset">Reset</button>
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                              </form>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      
                    </div>
                    <!-- end of accordion -->

                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Data Diri</th>
                          <th>Pendaftaran</th>
                          <th class="no-sort">Status</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php for ($i = 0; $i < 30; $i++){ 
                      $j=$i+1;
                      $k=$i+2019;
                      echo '
                        <tr>
                          <td>
                            <div>2019140001</div>
                            <div>Salahuddin Yusuf</div>
                            <div>Laki-laki</div>
                          </td>
                          <td>
                            <div>Gelombang '.$k.'-  Gel.'.$j.'</div>
                            <div>Jenjang D3 Farmasi</div>
                            <div>Jenis daftar: Reguler</div>
                          </td>
                          <td>
                            <div>Data: <span class="success">Aktif</span></div>
                            <div>Bayar Daftar: <span class="warning">Checking</span></div>
                            <div>Bayar Kuliah: </div>
                            <div>Pendaftaran: </div>
                          </td>
                          <td>
                            <a href="list_peserta.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                            <a href="edit_peserta.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <div>2019140002</div>
                            <div>Saladina Yusie</div>
                            <div>Perempuan</div>
                          </td>
                          <td>
                          <div>Gelombang '.$k.'-  Gel.'.$j.'</div>
                            <div>Jenjang D3 Farmasi</div>
                            <div>Jenis daftar: Reguler</div>
                          </td>
                          <td>
                            <div>Data: <span class="success">Aktif</span></div>
                            <div>Bayar Daftar: <span class="warning">Checking</span></div>
                            <div>Bayar Kuliah: </div>
                            <div>Pendaftaran: </div>
                          </td>
                          <td>
                            <a href="list_peserta.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                            <a href="edit_peserta.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>';
                      }?>
                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>