<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Jadwal Kuliah</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="perkuliahan.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Jadwal Kuliah</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="mt15">

                <table id="" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Progam Studi</th>
                      <th class="no-sort">Ketua Program Studi</th>
                      <th class="no-sort">Action</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tbody>
                        <tr>
                          <td>1</td>
                          <td>S1 Farmasi</td>
                          <td>Dina Pratiwi</td>
                          <td>
                            <a href="jadwal_kuliah-semester.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                          </td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>D3 Farmasi</td>
                          <td>Meta Safitri</td>
                          <td>
                            <a href="jadwal_kuliah-semester.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                          </td>
                        </tr>
                    </tbody>

                </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>