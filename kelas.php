<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kelas</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="mahasiswa.php">Mahasiswa</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kelas</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <!-- start accordion -->
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Filter</h4>
                          
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <div class="row">
                              <form id="" class="form-horizontal form-label-left">

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Nama Kelas
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Proram Studi
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Prodi 1</option>
                                          <option value="">Prodi 2</option>
                                          <option value="">Prodi 3</option>
                                          <option value="">Prodi 4</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>     

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Dosen Pembimbing
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Angkatan
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                  </div>    
                                  
                                  <div class="clearfix"></div>
                                  <div class="ln_solid"></div>

                                  <div class="form-group">
                                    <div class="col-md-12 col-sm-6 col-xs-12 center">
                                       <button class="btn btn-primary" type="reset">Reset</button>
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                              </form>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      
                    </div>
                    <!-- end of accordion -->
            
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-kelas"><i class="fa fa-plus-circle"></i> Tambah Kelas</button>

            <!-- tambah kelas  -->
            <div class="modal fade tambah-kelas" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Kelas</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Progam Studi
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Prodi 1</option>
                            <option value="">Prodi 2</option>
                            <option value="">Prodi 3</option>
                            <option value="">Prodi 4</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Angkatan
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Kelas
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jenis Kelas
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Kelas 1</option>
                            <option value="">Kelas 2</option>
                            <option value="">Kelas 3</option>
                            <option value="">Kelas 4</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Dosen PA
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Dosen 1</option>
                            <option value="">Dosen 2</option>
                            <option value="">Dosen 3</option>
                            <option value="">Dosen 4</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Angkatan PMB
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Angkatan 1</option>
                            <option value="">Angkatan 2</option>
                            <option value="">Angkatan 3</option>
                            <option value="">Angkatan 4</option>
                          </select>
                        </div>
                      </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah</button>
                  </div>

                </div>
              </div>
            </div>

            <div class="mt15">

                <table id="" class="datatable table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Angkatan</th>
                      <th>Program Studi</th>
                      <th>Kelas</th>
                      <th>Pembimbing Akademik</th>
                      <th class="no-sort">Action</th>
                    </tr>
                  </thead>

                    <tbody>
                      <?php for ($i = 0; $i < 50; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>14</td>
                          <td>
                            S1 Farmasi - Reguler
                          </td>
                          <td>
                            <div>14S1 A</div>
                            <div>14S1 B</div>
                          </td>
                          <td>
                            <div>Abdul Aziz Setyawan</div>
                            <div>Meta Safitri</div>
                          </td>
                          <td>
                            <a href="detail_kelas-mahasiswa.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> List Mahasiswa</a>
                          </td>
                        </tr>';
                      }?>
                    </tbody>

                </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>