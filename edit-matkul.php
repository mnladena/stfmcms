<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Perkuliahan</h3>
                <h4>Program Studi S1 Farmasi</h4>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="perkuliahan.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Perkuliahan</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-9 right">
                <a href="perkuliahan-kelas.php" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
              </div>  
            </div>

            <!-- pilih dosen  -->
            <div class="modal fade pilih-dosen" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Pilih Dosen</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Target Waktu
                        </label>
                        <div class="control-label text-left col-md-1 col-sm-1 col-xs-12"><div class="text-center">:</div></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="inp-text">
                            Kamis 08.00 - 09.30
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Cari Dosen
                        </label>
                        <div class="control-label text-left col-md-1 col-sm-1 col-xs-12"><div class="text-center">:</div></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input list="dosen" class="form-control col-md-6 col-xs-12">
                          <datalist id="dosen">
                            <option value="Aji">
                            <option value="Amoroso">
                            <option value="Andi">
                            <option value="Bambang">
                            <option value="Clara">
                            <option value="Diana">
                            <option value="Dono">
                          </datalist>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Dosen
                        </label>
                        <div class="control-label text-left col-md-1 col-sm-1 col-xs-12"><div class="text-center">:</div></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="inp-text">
                            [nama dosennya]
                          </div>
                        </div>
                      </div>
                      
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Mata Kuliah</th>
                            <th>Waktu</th>
                            <th>Ruangan</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Farmasi Fisika</td>
                            <td>
                              <div>Rabu</div>
                              <div>07.00 - 09.00</div>
                            </td>
                            <td>
                              <div>Gedung STFM</div>
                              <div>L302</div>
                            </td>
                          </tr>
                          <tr>
                            <td>Farmasi Fisika</td>
                            <td>
                              <div>Kamis</div>
                              <div>13.00 - 15.00</div>
                            </td>
                            <td>
                              <div>Gedung STFM</div>
                              <div>L302</div>
                            </td>
                          </tr>
                        </tbody>
                      </table>

                      <div class="alert alert-success alert-dismissible text-center">Dosen Tersedia</div>

                      <div class="alert alert-danger alert-dismissible text-center">Jadwal Dosen Bentrok</div>


                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button disabled type="button" class="btn btn-primary">Submit</button>
                  </div>

                </div>
              </div>
            </div>


            <!-- pilih ruangan  -->
            <div class="modal fade pilih-ruangan" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Pilih Ruangan</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Target Waktu
                        </label>
                        <div class="control-label text-left col-md-1 col-sm-1 col-xs-12"><div class="text-center">:</div></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="inp-text">
                            Kamis 08.00 - 09.30
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Cari Ruangan
                        </label>
                        <div class="control-label text-left col-md-1 col-sm-1 col-xs-12"><div class="text-center">:</div></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input list="ruangan" class="form-control col-md-6 col-xs-12">
                          <datalist id="ruangan">
                            <option value="Gedung STFM L301">
                            <option value="Gedung STFM L302">
                            <option value="Gedung STFM L303">
                            <option value="Gedung STFM L304">
                            <option value="Gedung STFM L402">
                            <option value="Gedung STFM L501">
                            <option value="Gedung STFM L602">
                          </datalist>
                        </div>
                      </div>
                      
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Nama Ruangan</th>
                            <th>Waktu Kosong</th>
                            <th>Waktu Terpakai</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <div>Gedung STFM</div>
                              <div>L302</div>
                            </td>
                            <td>
                              03.00
                            </td>
                            <td>
                              04.00
                            </td>
                            <td>
                              <div class="radio pilihan-jawaban">
                                <input type="radio" class="flat left" value="" id="" name="">
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Ruangan Dipilih
                        </label>
                        <div class="control-label text-left col-md-1 col-sm-1 col-xs-12"><div class="text-center">:</div></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="inp-text">
                            [nama ruangannya]
                          </div>
                        </div>
                      </div>


                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button disabled type="button" class="btn btn-primary">Submit</button>
                  </div>

                </div>
              </div>
            </div>

            <div class="mt15">

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title text-center">
                            <h4>Pengaturan Jadwal Kuliah</h4>
                          </div>
                          <div class="x_content">
                            <table class="table_info-noborder">
                              <tr>
                                <td>Kode Mata Kuliah</td>
                                <td width="50" align="center">:</td>
                                <td>MKB-5215</td>
                              </tr>
                              <tr>
                                <td>Mata Kuliah</td>
                                <td width="50" align="center">:</td>
                                <td>Obat Bahan Alam - 2/0 SKS</td>
                              </tr>
                              <tr>
                                <td>Jenis Mata Kuliah</td>
                                <td width="50" align="center">:</td>
                                <td>Mata Kuliah Wajib</td>
                              </tr>
                            </table>

                            <hr>

                            <form id="tambah-gelombang" class="form-horizontal form-label-left">

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Hari
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                  <select id="" class="form-control">
                                    <option value="">Pilih Hari..</option>
                                    <option value="">Senin</option>
                                    <option value="">Selasa</option>
                                    <option value="">Rabu</option>
                                    <option value="">Kamis</option>
                                    <option value="">Jumat</option>
                                    <option value="">Sabtu</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Waktu
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                  <div class="row">
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                      <input type="time" id="" class="form-control col-md-6 col-xs-12" value="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12"><span class="inp-text full center">s/d</span></div>
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                      <input type="time" id="" class="form-control col-md-6 col-xs-12" value="">
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Dosen
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                  <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <div class='btn-wrap'>
                                    <div class="btn btn-add" data-toggle="modal" data-target=".pilih-dosen"><i class="fa fa-plus-square"></i></div>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Ruangan
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                  <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                                </div>
                                <div class='btn-wrap'>
                                    <div class="btn btn-add" data-toggle="modal" data-target=".pilih-ruangan"><i class="fa fa-plus-square"></i></div>
                                </div>
                              </div>

                              <div class="ln_solid"></div>

                              <div class="form-group">
                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                      <a href="list_peserta.php" class="btn btn-default" type="button">Batal</a>
                                      <button class="btn btn-primary" type="submit">Simpan</button>
                                   </div>
                                 </div>

                            </form>

                          </div>
                        </div>
                    </div>
                </div>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>