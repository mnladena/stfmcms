<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kurikulum</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kurikulum</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-kurikulum"><i class="fa fa-plus-circle"></i> Tambah Kurikulum</button>

            <div class="mt15">

            <!-- tambah kurikulum  -->
            <div class="modal fade tambah-kurikulum" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Kurikulum</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Kurikulum
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Program Studi
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">D3 Farmasi</option>
                            <option value="">S1 Farmasi</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit kurikulum  -->
            <div class="modal fade edit-kurikulum" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Kurikulum</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama Kurikulum
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control" value="Kurikulum D3">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Program Studi
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option selected value="">D3 Farmasi</option>
                            <option value="">S1 Farmasi</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- lock kurikulum  -->
            <div class="modal fade lock-kurikulum" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Kunci Kurikulum</h4>
                  </div>

                  <div class="modal-body center">
                    <p>
                      Apakah Anda yakin ingin mengunci kurikulum di bawah ini?*
                      <div>
                        <strong>Kurikulum D3</strong>
                      </div>
                      <div class="mt15 text-danger">*Kurikulum yang terkunci tidak bisa diubah lagi</div>
                    </p>
                  </div>

                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Hapus</button>
                  </div>

                </div>
              </div>
            </div>

                    <table id="" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Kurikulum</th>
                          <th>Program Studi</th>
                          <th>Tahun Pembuatan</th>
                          <th>Tanggal Aktif</th>
                          <th>Status</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Kurikulum D3</td>
                          <td>D3 Farmasi</td>
                          <td>2019</td>
                          <td>01/11/2019</td>
                          <td>
                            <div><strong>Terkunci</strong></div>
                            <div class="text-success">Aktif</div>
                          </td>
                          <td>
                              <button data-toggle="modal" data-target=".edit-kurikulum" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                              <a href="detail_kurikulum.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                          </td>
                        </tr>

                        <tr>
                          <td>2</td>
                          <td>Kurikulum S1</td>
                          <td>S1 Farmasi</td>
                          <td>2019</td>
                          <td>01/11/2019</td>
                          <td>
                            <div>Terbuka</div>
                            <div class="text-danger">Tidak Aktif</div>
                          </td>
                          <td>
                              <button data-toggle="modal" data-target=".lock-kurikulum" class="btn btn-xs btn"><i class="fa fa-lock"></i> Lock</button>
                              <button data-toggle="modal" data-target=".edit-kurikulum" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                              <a href="detail_kurikulum.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                          </td>
                        </tr>

                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>