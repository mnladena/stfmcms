<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Pembiayaan Pendukung Perkuliahan</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <div class="" role="tabpanel" data-example-id="togglable-tabs">

              <?php $title = array("Registrasi PMB", "Admin PMB", "Daftar Ulang", "Sidang", "Wisuda","Cuti","Sewa Lab"); ?>


              <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                <?php for ($i = 0; $i < 7; $i++){ 
                   $j=$i+1;
                        echo '
                <li role="presentation" class="'.(($j=='1')?'active':"").'"><a href="#tab_content'.$j.'" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">'.$title[$i].'</a>
                </li>'; }?>
              </ul>

              <div id="myTabContent" class="tab-content">

                <?php for ($i = 0; $i < 7; $i++){ 
                        $j=$i+1;
                        echo '
                <div role="tabpanel" class="tab-pane fade '.(($j=='1')?'active in':"").'" id="tab_content'.$j.'" aria-labelledby="d3-tab">
                  
                  <div class="mt15">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-biaya-reg"><i class="fa fa-plus-circle"></i> Tambah</button>

                    <div class="mt15">
                      <table id="" class="table table-striped table-bordered datatable">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th class="no-sort">Biaya</th>
                            <th class="no-sort">Tanggal Pembuatan</th>
                            <th class="no-sort">Tanggal Aktif</th>
                            <th class="no-sort">Status</th>
                            <th class="no-sort">Action</th>
                          </tr>
                        </thead>


                        <tbody>';

                          for ($s = 0; $s < 30; $s++){ 
                        $k=$s+1;
                        echo '
                          <tr>
                            <td>
                              '.$k.'
                            </td>
                            <td>
                              Rp 500.000
                            </td>
                            <td>
                               09/08/2019
                            </td>
                            <td>
                               15/09/2019
                            </td>
                            <td>
                              Aktif
                            </td>
                            <td>
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".edit-biaya-reg"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                          </tr>';}

                          echo'
                        </tbody>
                      </table>
                    </div>

                  </div>

                </div>';
                        }?>

                
            </div>

          </div>

          <!-- tambah biaya registrasi -->
            <div class="modal fade tambah-biaya-reg" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Biaya Registrasi PMB</h4>
                  </div>
                  <div class="modal-body">
                    <form id="demo-form2" class="form-horizontal form-label-left box">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Biaya
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" class="form-control col-md-7 col-xs-12" value="">
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input required="required" type="radio" class="flat left" checked value="jenjang1" id="jenjang1" name="jenjang"> <span class="inp-text left">Aktif</span>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input required="required" type="radio" class="flat left" value="jenjang2" id="jenjang2" name="jenjang"> <span class="inp-text left">Tidak Aktif</span>
                        </div>
                      </div>
                    </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit biaya registrasi -->
            <div class="modal fade edit-biaya-reg" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Biaya Registrasi PMB</h4>
                  </div>
                  <div class="modal-body">
                    <form id="demo-form2" class="form-horizontal form-label-left box">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Biaya
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" disabled="" class="form-control col-md-7 col-xs-12" value="300.000">
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input required="required" type="radio" class="flat left" checked value="jenjang1" id="jenjang1" name="jenjang"> <span class="inp-text left">Aktif</span>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input required="required" type="radio" class="flat left" value="jenjang2" id="jenjang2" name="jenjang"> <span class="inp-text left">Tidak Aktif</span>
                        </div>
                      </div>
                    </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>
          

            
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>