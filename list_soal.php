<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Soal Ujian versi 2019</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Soal Ujian</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Detail</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="soal-desc">
              <div class="row">
                <div class="kolom col-md-5 col-sm-5">Soal Matematika: 15</div>
                <div class="kolom col-md-5 col-sm-5">Soal Bahasa Indonesia: 20</div>
                <div class="kolom col-md-5 col-sm-5">Soal Bahasa Inggris: 15</div>
                <div class="kolom col-md-5 col-sm-5">Soal Gambar: 5</div>
              </div>
            </div>

            <!-- start accordion -->
            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

              <div class="panel">
                <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <h4 class="panel-title">Filter</h4>
                  
                </a>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="panel-body">
                    <div class="row">
                      <form id="" class="form-horizontal form-label-left">
                          <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="form-group">
                              <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Jenis Soal
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="" class="form-control">
                                  <option value="">Choose..</option>
                                  <option value="">Matematika</option>
                                  <option value="">B. Indonesia</option>
                                  <option value="">B. Ingggris</option>
                                  <option value="">Gambar</option>
                                </select>
                              </div>
                            </div>

                          </div>     

                          <div class="col-md-6 col-sm-6 col-xs-12">

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pertanyaan
                                </label>
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                  <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>

                          </div>    
                          
                          <div class="clearfix"></div>
                          <div class="ln_solid"></div>

                          <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12 center">
                              <button class="btn btn-primary" type="reset">Reset</button>
                              <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                          </div>

                      </form>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>

              </div>
              <!-- end of accordion -->



            <a class="btn btn-primary" href="tambah_soal.php"><i class="fa fa-plus-circle"></i> Tambah Soal</a>


            <div class="mt15">

                    <table id="" class="table table-striped table-bordered datatable">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Jenis Soal</th>
                          <th>Sort</th>
                          <th class="no-sort">Pertanyaan</th>
                          <th class="no-sort">Jawaban</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>


                      <tbody>

                      <?php for ($i = 0; $i < 30; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>
                            Matematika
                          </td>
                          <td>'
                          .$j.
                          '</td>
                          <td>
                          <div class="text-soal">
                            Hasil dari 4+3x50 adalah
                          </div>
                          </td>
                          <td>
                            <div class="answer-list"><span class="correct">A</span> <div class="text-answer">9</div></div>
                            <div class="answer-list"><span>B</span> <div class="text-answer">19</div></div>
                            <div class="answer-list"><span>C</span> <div class="text-answer">20</div></div>
                            <div class="answer-list"><span>D</span> <div class="text-answer">13</div></div>
                            <div class="answer-list"><span>E</span> <div class="text-answer">77</div></div>
                          </td>
                          <td>
                            <a href="edit_soal.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>
                        <tr>
                          <td>'.$j.'</td>
                          <td>
                            Bahasa Indonesia
                          </td>
                          <td>'.$j.'</td>
                          <td>
                            <div class="text-soal">
                              Air beriak tanda tak dalam artinya..
                            </div>
                          </td>
                          <td>
                            <div class="answer-list"><span>A</span> <div class="text-answer">Pendiam yang tulus abadi</div></div>
                            <div class="answer-list"><span>B</span> <div class="text-answer">Orang yang merasa paling benar sedunia</div></div>
                            <div class="answer-list"><span>C</span> <div class="text-answer">Terkoyak oleh semu dunia yang fana</div></div>
                            <div class="answer-list"><span>D</span> <div class="text-answer">Berkelana mencari cinta</div></div>
                            <div class="answer-list"><span class="correct">E</span> <div class="text-answer">Tidak tahu</div></div>
                          </td>
                          <td>
                            <a href="edit_soal.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>
                        <tr>
                          <td>'.$j.'</td>
                          <td>
                            Bahasa Inggris
                          </td>
                          <td>'.$j.'</td>
                          <td>
                            <div class="text-soal">
                              When you are smiling, people will smiling back...
                            </div>
                          </td>
                          <td>
                            <div class="answer-list"><span>A</span> <div class="text-answer">to the future</div></div>
                            <div class="answer-list"><span>B</span> <div class="text-answer">street back allright!</div></div>
                            <div class="answer-list"><span class="correct">C</span> <div class="text-answer">at you</div></div>
                            <div class="answer-list"><span>D</span> <div class="text-answer">packer real world</div></div>
                            <div class="answer-list"><span>E</span> <div class="text-answer">in to you</div></div>
                          </td>
                          <td>
                            <a href="edit_soal.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>
                        <tr>
                          <td>'.$j.'</td>
                          <td>
                            Gambar
                          </td>
                          <td>'.$j.'</td>
                          <td>
                            <div class="text-soal">
                              <img src="images/gbr.jpg" alt="" width="300">
                            </div>
                          </td>
                          <td>
                            <div class="answer-list"><span>A</span> <div class="text-answer"><img data-toggle="modal" data-target=".jawaban-gambar" src="images/gbr.jpg"> </div></div>
                            <div class="answer-list"><span>B</span> <div class="text-answer"><img data-toggle="modal" data-target=".jawaban-gambar" src="images/gbr.jpg"> </div></div>
                            <div class="answer-list"><span class="correct">C</span> <div class="text-answer"><img data-toggle="modal" data-target=".jawaban-gambar" src="images/gbr.jpg"> </div></div>
                            <div class="answer-list"><span>D</span> <div class="text-answer"><img data-toggle="modal" data-target=".jawaban-gambar" src="images/gbr.jpg"> </div></div>
                            <div class="answer-list"><span>E</span> <div class="text-answer"><img data-toggle="modal" data-target=".jawaban-gambar" src="images/gbr.jpg"> </div></div>
                          </td>
                          <td>
                            <a href="edit_soal.php" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>
                        ';
                      }?>

                      </tbody>
                    </table>
                    
                    <!-- popup image answer -->
                    <div class="modal fade jawaban-gambar" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-md">
                        <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                          <div class="modal-body center">
                            <img src="images/gbr.jpg" alt="">
                          </div>
                          <div class="modal-footer answer-list center">
                            <a class="active" href="#">A</a>
                            <a href="#">B</a>
                            <a class=" correct" href="#">C</a>
                            <a href="#">D</a>
                            <a href="#">E</a>
                        </div>
                        </div>
                      </div>
                    </div>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>