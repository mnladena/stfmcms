<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Log Aktifitas</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Log Aktifitas</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="">

              <!-- start accordion -->
              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Filter</h4>
                          
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <div class="row">
                              <form id="" class="form-horizontal form-label-left">

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Waktu
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class='input-group date'>
                                            <input required="required" type='text' class="form-control"  id='datetimepicker6' value="20/08/1995"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                      </div>
                                    </div>


                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">User
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="" name="" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Status User
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Dosen</option>
                                          <option value="">Mahasiswa</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>     

                                  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Sistem
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Siak</option>
                                          <option value="">Siakad</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Menu
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">KRS</option>
                                          <option value="">KHS</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-6 col-sm-6 col-xs-12" for="">Aksi
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="" class="form-control">
                                          <option value="">Choose..</option>
                                          <option value="">Submit</option>
                                          <option value="">Edit</option>
                                          <option value="">Add</option>
                                          <option value="">Delete</option>
                                        </select>
                                      </div>
                                    </div>

                                  </div>    
                                  
                                  <div class="clearfix"></div>
                                  <div class="ln_solid"></div>

                                  <div class="form-group">
                                    <div class="col-md-12 col-sm-6 col-xs-12 center">
                                       <button class="btn btn-primary" type="reset">Reset</button>
                                       <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                              </form>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      
                    </div>
                    <!-- end of accordion -->

              <div class="mt15">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Waktu</th>
                          <th>User</th>
                          <th>Sistem</th>
                          <th>Menu</th>
                          <th>Aksi</th>
                          <th>Aktifitas</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php for ($i = 0; $i < 30; $i++){ 
                      $j=$i+1;
                      $k=$i+2019;
                      echo '
                        <tr>
                          <td>
                            '.$j.'
                          </td>
                          <td>
                            <div>20/08/2019</div>
                            <div>09.00</div>
                          </td>
                          <td>
                            <div>Salahuddin Yusuf</div>
                            <div>Mahasiswa</div>
                          </td>
                          <td>
                            SIAK
                          </td>
                          <td>
                            KRS
                          </td>
                          <td>
                            Submit
                          </td>
                          <td>
                            Salahuddin Yusuf sebagai mahasiswa mensubmit KRS tahun ajaran 2019/2020
                          </td>
                        </tr>';
                      }?>
                      </tbody>
                    </table>
                  </div>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>