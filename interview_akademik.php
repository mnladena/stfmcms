<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_center">
                <h3>Interview Penilaian AIK</h3>
              </div>

              </div>

          </div>

            <div class="clearfix"></div>

            <div class="interview_desc">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="kolom col-md-3 col-sm-3">
                    <img src="images/img.jpg" alt="">
                  </div>
                  <div class="kolom col-md-5 col-sm-5">
                      <div class="col-md-4 col-sm-4">NIRM</div><div class="col-md-2 col-sm-2">:</div><div class="col-md-4 col-sm-4">03450001</div>
                      <div class="col-md-4 col-sm-4">Nama</div><div class="col-md-2 col-sm-2">:</div><div class="col-md-4 col-sm-4">Ruslan Amir</div>
                      <div class="col-md-4 col-sm-4">Asal Sekolah</div><div class="col-md-2 col-sm-2">:</div><div class="col-md-4 col-sm-4">SMAN 1 Tangerang</div>
                      <div class="col-md-4 col-sm-4">Jenjang</div><div class="col-md-2 col-sm-2">:</div><div class="col-md-4 col-sm-4">S1 Farmasi</div>
                      <div class="col-md-4 col-sm-4">Jenis Daftar</div><div class="col-md-2 col-sm-2">:</div><div class="col-md-4 col-sm-4">Regular</div>
                  </div>
                  <div class="kolom col-md-2 col-sm-2 pull-right">11 Juli 2019</div>
                </div>
              </div>
            </div>


            <div class="mt15">
              <form id="" class="form-horizontal">
                    <table id="" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Aspek</th>
                          <th>Uraian</th>
                          <th>Nilai</th>
                          <th>Kategori</th>
                        </tr>
                      </thead>


                      <tbody>
                      <tr>
                        <td>1</td>
                        <td>Penampilan Fisik</td>
                        <td>Badan sehat, bersih, wajah bersemangat</td>
                        <td>
                          <select id="" class="form-control" required>
                            <option value="">Choose..</option>
                            <option value="">10</option>
                            <option value="">20</option>
                            <option value="">30</option>
                            <option value="">40</option>
                          </select>
                        </td>
                        <td>Kurang</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Sopan santun</td>
                        <td>Menyapa dengan baik, duduk setelah diperilahkan</td>
                        <td>
                          <select id="" class="form-control" required>
                            <option value="">Choose..</option>
                            <option value="">10</option>
                            <option value="">20</option>
                            <option value="">30</option>
                            <option value="">40</option>
                          </select>
                        </td>
                        <td>Cukup</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Prestasi Akademik</td>
                        <td>Uraian</td>
                        <td>
                          <select id="" class="form-control" required>
                            <option value="">Choose..</option>
                            <option value="">10</option>
                            <option value="">20</option>
                            <option value="">30</option>
                            <option value="">40</option>
                          </select>
                        </td>
                        <td>Cukup</td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Pemahaman mengenai dunia kesehatan</td>
                        <td>Uraian</td>
                        <td>
                          <select id="" class="form-control" required>
                            <option value="">Choose..</option>
                            <option value="">10</option>
                            <option value="">20</option>
                            <option value="">30</option>
                            <option value="">40</option>
                          </select>
                        </td>
                        <td>Cukup</td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Kemampuan finansial</td>
                        <td>Uraian</td>
                        <td>
                          <select id="" class="form-control" required>
                            <option value="">Choose..</option>
                            <option value="">10</option>
                            <option value="">20</option>
                            <option value="">30</option>
                            <option value="">40</option>
                          </select>
                        </td>
                        <td>Cukup</td>
                      </tr>
                      </tbody>

                    </table>

                    <div>
                      <div class="title">Riwayat Kesehatan</div>
                      <table class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>Uraian</th>
                            <th>Isi</th>
                            <th>Keterangan</th>
                          </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td rowspan="2">Pernah/sedang menderita penyakit</td>
                          <td>
                            <div class="radio pilih">
                                <input type="radio" class="flat left" value="alamat1" id="alamat1" name="alamat"> <label for="">Ya</label>
                            </div>
                          </td>
                          <td rowspan="2">
                            <textarea id="" class="form-control" name="" rows=5></textarea>
                          </td>
                        </tr>
                        <tr>
                          <td>
                              <div class="radio pilih">
                                  <input type="radio" class="flat left" value="alamat1" id="alamat1" name="alamat"> <label for="">Tidak</label>
                              </div>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                      
                    </div>

                    <div class="mt15"></div>
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                          <table class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th class="center" colspan="2">Kesimpulan</th>
                              </tr>
                            </thead>
                            <tbody>
                            <tr>
                              <td><label>Dapat disarankan</label></td>
                              <td>
                                  <div class="radio pilih">
                                    <input type="radio" class="flat left" value="alamat1" id="alamat1" name="alamat">
                                  </div>
                              </td>
                            </tr>
                            <tr>
                              <td><label>Masih dapat disarankan</label></td>
                              <td>
                                <div class="radio pilih">
                                    <input type="radio" class="flat left" value="alamat1" id="alamat1" name="alamat">
                                  </div>
                              </td>
                            </tr>
                            <tr>
                              <td><label>Tidak dapat disarankan</label></td>
                              <td>
                                <div class="radio pilih">
                                    <input type="radio" class="flat left" value="alamat1" id="alamat1" name="alamat">
                                  </div>
                              </td>
                            </tr>
                            </tbody>
                          </table>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12"></div>

                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <table class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th class="center" colspan="2">Pewawancara</th>
                            </tr>
                          </thead>
                          <tbody>
                            <td>Salahuddin Yusuf ST</td>
                            <td>
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".cari-interview"><i class="fa fa-search"></i> cari</button>
                            </td>
                          </tr>
                          </tbody>
                        </table>

                        <table class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th class="center" colspan="2">Asisten Pewawancara</th>
                            </tr>
                          </thead>
                          <tbody>
                            <td>Salahuddin Yusuf ST</td>
                            <td>
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".cari-interview"><i class="fa fa-search"></i> cari</button>
                            </td>
                          </tr>
                          </tbody>
                        </table>

                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group center">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <button class="btn btn-primary" type="button">Kembali</button>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target=".submit-interview">Submit</button>
                      </div>
                    </div>


                  <!-- popup cari -->
                  <div class="modal fade cari-interview" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Cari Pewawancara/Asisten Pewawancara</h4>
                        </div>
                        <div class="modal-body">

                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <strong>Error!</strong> Notifikasi error.
                        </div>

                          <form id="" class="form-horizontal">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                  Nama
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                  :
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  Salahuddin Yusuf ST
                                </div>
                                <div class="col-md-2 col-sm-3 col-xs-12">
                                  <button class="btn btn-primary">Pilih</button>
                                </div>
                              </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="ln_solid"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                  NIDN
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                  :
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="" name="" class="form-control col-md-12 col-xs-12">
                                </div>
                              </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                  Nama Dosen
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                  :
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  Nama Dosen
                                </div>
                              </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                  Password
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                  :
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="" name="" class="form-control col-md-12 col-xs-12">
                                </div>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer center ">
                          <button type="button" class="btn btn-primary">Pilih</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>

                  
                    
                    <!-- popup submit -->
                    <div class="modal fade submit-interview" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel2">Konfirmasi Submit</h4>
                          </div>
                          <div class="modal-body">
                            Apakah Anda yakin ingin mensubmit penilaian interview?
                          </div>
                          <div class="modal-footer center ">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>

                        </div>
                      </div>
                    </div>

                </form>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>