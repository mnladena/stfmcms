<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Soal Interview</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".create-interview"><i class="fa fa-plus-circle"></i> Tambah Versi</button>

            <div class="mt15">

            <!-- create  -->
            <div class="modal fade create-interview" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Versi</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label" for="">Waktu Pembuatan
                        </label>
                        <div class="">
                          <span class="inp-text">Kamis, 4 juli 2019</span> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Nama Versi
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Deskripsi
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5></textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit -->
            <div class="modal fade edit-interview" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Versi</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label" for="">Waktu Pembuatan
                        </label>
                        <div class="">
                          <span class="inp-text">Kamis, 4 juli 2019</span> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Nama Versi
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="2019">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Deskripsi
                        </label>
                        <div class="">
                          <textarea id="message" class="form-control" name="" rows=5>Marilah seluruh rakyat Indonesia</textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

                    <table id="" class="table table-striped table-bordered datatable">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Versi</th>
                          <th>Deskripsi</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>

                      <?php for ($i = 0; $i < 30; $i++){ 
                      $j=$i+1;
                      $k=$i+2019;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>
                            <div>Versi '.$k.'</div>
                            <div>Pembuatan: 1 Juli 2019</div>
                            <div>Update: 5 Juli 2019</div>
                          </td>
                          <td>-</td>
                          <td>
                            <a href="list_interview.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                            <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".edit-interview"><i class="fa fa-edit"></i> Edit</a>
                            <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                        </tr>';
                      }?>

                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>