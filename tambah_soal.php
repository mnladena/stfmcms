<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Soal Ujian versi 2019</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Soal Ujian</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Detail</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="soal-desc">
              <div class="row">
                <div class="kolom col-md-5 col-sm-5">Soal Matematika: 15</div>
                <div class="kolom col-md-5 col-sm-5">Soal Bahasa Indonesia: 20</div>
                <div class="kolom col-md-5 col-sm-5">Soal Bahasa Inggris: 15</div>
                <div class="kolom col-md-5 col-sm-5">Soal Gambar: 5</div>
              </div>
            </div>

            <div class="mt15">

            <form id="tambah-soal" class="form-horizontal form-label-left box">

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pertanyaan ke
                </label>
                <div class="col-md-1 col-sm-1 col-xs-12">
                  <input type="text" id="" disabled class="form-control col-md-7 col-xs-12" value="20">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Urutan
                </label>
                <div class="col-md-1 col-sm-1 col-xs-12">
                  <input type="text" id="" disabled class="form-control col-md-7 col-xs-12" value="20">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jenis Pertanyaan
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select id="" class="form-control">
                    <option value="">Choose..</option>
                    <option value="">Matematika</option>
                    <option value="">B. Indonesia</option>
                    <option value="">B. Ingggris</option>
                    <option value="">Gambar</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pertanyaan
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea id="" class="form-control texteditor" name="" rows=8></textarea>
                </div>
              </div>

              <br>
              <hr>
              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Jawaban
                </label>

                <div class="col-md-6 col-sm-6 col-xs-12">

                  <div class="jawaban-soal">

                    <div class="list-jawaban">
                      <div class="radio pilihan-jawaban">
                          <div>
                            <input type="radio" class="flat left" checked value="jawaban1" id="jawaban1" name="jawaban"> <span class="inp-text left">A</span>
                            <textarea id="" class="form-control texteditor" name="" rows=8></textarea>
                          </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>
                  </div>

                  <div class="col-md-12">
                    <button class="tambah-jwb btn btn-primary pull-right"> <i class="fa fa-plus"></i> Tambah Jawaban</button>
                    <button class="del-jwb btn btn-danger pull-right"> <i class="fa fa-minus"></i> Delete Jawaban</button>
                  </div>

                  <!-- <div class="list-jawaban">
                    <div class="radio pilihan-jawaban">
                        <div>
                          <input type="radio" class="flat left" value="jawaban2" id="jawaban2" name="jawaban"> <span class="inp-text left">B</span>
                          <textarea id="" class="form-control texteditor" name="" rows=8></textarea>
                        </div>
                    </div>
                  </div>

                  <div class="list-jawaban">
                    <div class="radio pilihan-jawaban">
                        <div>
                          <input type="radio" class="flat left" value="jawaban3" id="jawaban3" name="jawaban"> <span class="inp-text left">C</span>
                          <textarea id="" class="form-control texteditor" name="" rows=8></textarea>
                        </div>
                    </div>
                  </div>

                  <div class="list-jawaban">
                    <div class="radio pilihan-jawaban">
                        <div>
                          <input type="radio" class="flat left" value="jawaban4" id="jawaban4" name="jawaban"> <span class="inp-text left">D</span>
                          <textarea id="" class="form-control texteditor" name="" rows=8></textarea>
                        </div>
                    </div>
                  </div>

                  <div class="list-jawaban">
                    <div class="radio pilihan-jawaban">
                        <div>
                          <input type="radio" class="flat left" value="jawaban5" id="jawaban5" name="jawaban"> <span class="inp-text left">E</span>
                          <textarea id="" class="form-control texteditor" name="" rows=8></textarea>
                        </div>
                    </div>
                  </div> -->

                </div>

              </div>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12 center">
                  <button class="btn" type="button">Cancel</button>
              <button class="btn btn-primary" type="reset">Simpan dan Tambah Soal Baru</button>
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>

              </form>

            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<script>

//tinymce
tinymce.init({
    selector: '.texteditor',
    menubar: false,
    plugins: 'image link lists imagetools',
    toolbar: 'formatselect | bold italic strikethrough | link image | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat ',
    height: 400,
    /* without images_upload_url set, Upload tab won't show up*/
  images_upload_url: 'postAcceptor.php',

/* we override default upload handler to simulate successful upload*/
images_upload_handler: function (blobInfo, success, failure) {
  setTimeout(function () {
    /* no matter what you upload, we will turn it into TinyMCE logo :)*/
    success('http://moxiecode.cachefly.net/tinymce/v9/images/logo.png');
  }, 2000);
}
  });


</script>