<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Jadwal Kuliah</h3>
                <h4>Program Studi S1 Farmasi</h4>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="perkuliahan.php">Akademik</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="jadwal_kuliah.php">Jadwal Kuliah</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Semester</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-9 col-sm-9 col-xs-9">
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <select id="" class="form-control">
                    <option value="">Choose..</option>
                    <option selected value="">2019/2020-Ganjil</option>
                    <option value="">2019/2020-Genap</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 right">
                <a href="jadwal_kuliah.php" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
              </div>  
            </div>

            <div class="mt15">

                <table id="" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Semester</th>
                      <th>Angkatan</th>
                      <th>Action</th>
                    </tr>
                  </thead>

                    <tbody>
                        <tr>
                          <td>
                            1
                          </td>
                          <td>Semester 1</td>
                          <td>
                            2020
                          </td>
                          <td>
                            <a href="jadwal_kuliah-matkul.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i>Detail</a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            1
                          </td>
                          <td>Semester 3</td>
                          <td>
                            2020
                          </td>
                          <td>
                            <a href="jadwal_kuliah-matkul.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i>Detail</a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            1
                          </td>
                          <td>Semester 5</td>
                          <td>
                            2020
                          </td>
                          <td>
                            <a href="jadwal_kuliah-matkul.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i>Detail</a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            1
                          </td>
                          <td>Semester 7</td>
                          <td>
                            2020
                          </td>
                          <td>
                            <a href="jadwal_kuliah-matkul.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i>Detail</a>
                          </td>
                        </tr>
                    </tbody>

                </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>