<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kurikulum</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kurikulum</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <h4>Kurikulum S1 Farmasi</h4>

            <div class="row">
              <div class="col-md-9 col-sm-9 col-xs-9">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-semester"><i class="fa fa-plus-circle"></i> Tambah Semester</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-matkul"><i class="fa fa-plus-circle"></i> Tambah Mata Kuliah</button>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 right">
                <a href="" class="btn btn-default"><i class="fa fa-file-pdf-o"></i> Download Kurikulum</a>
              </div>
              </div>

            <div class="clearfix"></div>

            <div class="mt15">

            <!-- tambah kurikulum  -->
            <div class="modal fade tambah-semester" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Semester</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Semester
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Semester 1</option>
                            <option value="">Semester 2</option>
                            <option value="">Semester 3</option>
                            <option value="">Semester 4</option>
                            <option value="">Semester 5</option>
                            <option value="">Semester 6</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Tambah</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- tambah matkul  -->
            <div class="modal fade tambah-matkul" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Mata Kuliah</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Semester
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Semester 1</option>
                            <option value="">Semester 2</option>
                            <option value="">Semester 3</option>
                            <option value="">Semester 4</option>
                            <option value="">Semester 5</option>
                            <option value="">Semester 6</option>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"></div>

                      <div class="mt15">
                        <table id="" class="datatable table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Kode Mata Kuliah</th>
                              <th>Nama Mata Kuliah</th>
                              <th class="no-sort">SKS</th>
                              <th class="no-sort">&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php for ($i = 0; $i < 30; $i++){ 
                            $j=$i+1;
                            echo '
                              <tr>
                                <td>'.$j.'</td>
                                <td>MPK-120'.$i.'</td>
                                <td>Aplikasi Sistem Komputer Farmasi</td>
                                <td>2/0</td>
                                <td>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" class="flat">
                                    </label>
                                  </div>
                                </td>
                              </tr>';
                            }?>
                          </tbody>
                        </table>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semester belum dipilih</div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- tambah prasyarat  -->
            <div class="modal fade tambah-prasyarat" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Prasyarat</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <h4>Mata Kuliah: Sidang Komprehensif</h4>

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">

                        <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="syarat1" role="tab" data-toggle="tab" aria-expanded="true">Syarat 1</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" id="syarat2" role="tab" data-toggle="tab" aria-expanded="false">Syarat 2</a>
                          </li>
                        </ul>

                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="berkas-tab">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Semester
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="" class="form-control">
                                  <option value="">Choose..</option>
                                  <option value="">Semester 1</option>
                                  <option value="">Semester 2</option>
                                  <option value="">Semester 3</option>
                                  <option value="">Semester 4</option>
                                  <option value="">Semester 5</option>
                                  <option value="">Semester 6</option>
                                </select>
                              </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="mt15">
                              <table id="datatable" class="datatable table table-striped table-bordered">
                                <thead>
                                  <tr>
                                    <th>Kode Mata Kuliah</th>
                                    <th>Nama Mata Kuliah</th>
                                    <th class="no-sort">SKS</th>
                                    <th class="no-sort">&nbsp;</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php for ($i = 0; $i < 30; $i++){ 
                                  $j=$i+1;
                                  echo '
                                    <tr>
                                      <td>MPK-120'.$i.'</td>
                                      <td>Aplikasi Sistem Komputer Farmasi</td>
                                      <td>2/0</td>
                                      <td>
                                        <div class="checkbox">
                                          <label>
                                            <input type="checkbox" class="flat">
                                          </label>
                                        </div>
                                      </td>
                                    </tr>';
                                  }?>
                                </tbody>
                              </table>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semester belum dipilih</div>
                          </div>

                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="berkas-tab">
                              <table class="table_info table_info-medium table_info-center table_info-noborder">
                                
                                <tr>
                                  <td>Jumlah SKS</td>
                                  <td>:</td>
                                   <td colspan="3" align="left">
                                    <div class="form-group">
                                      <input type="text" name="">
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Min.IPK</td>
                                  <td>:</td>
                                   <td colspan="3" align="left">
                                     <div class="form-group">
                                      <input type="text" name="">
                                    </div>
                                   </td>
                                </tr>
                                <tr>
                                  <td>Nilai</td>
                                  <td>:</td>
                                  <td>
                                    <div class="form-group">
                                      <input type="text" name="" placeholder="A/B/C/D/E">
                                    </div>
                                  </td>
                                  <td>Jumlah</td>
                                  <td>:</td>
                                  <td>
                                    <div class="form-group">
                                      <input type="text" name="">
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Nilai E</td>
                                  <td>:</td>
                                   <td colspan="3" align="left">
                                    <div class="checkbox">
                                          <label>
                                            <input type="checkbox" class="flat">
                                          </label> Tidak ada nilai E
                                        </div>
                                      </td>
                                </tr>
                                
                              </table>

                          </div>
                        </div>

                      </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- lock kurikulum  -->
            <div class="modal fade prasyarat-matkul" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Prasyarat</h4>
                  </div>

                  <div class="modal-body center">
                    <h3>Prasyarat Sidang Komprehensif</h3>
                    <table class="table_info table_info-medium table_info-center table_info-noborder">
                      <tr>
                        <td width="40%">Mata Kuliah</td>
                        <td width="2%">:</td>
                        <td colspan="3" align="left">
                          <div>Sidang AIK</div>
                          <div>Sidang Skripsi</div>
                        </td>
                      </tr>
                      <tr>
                        <td>Jumlah SKS</td>
                        <td>:</td>
                         <td colspan="3" align="left">120</td>
                      </tr>
                      <tr>
                        <td>Min.IPK</td>
                        <td>:</td>
                         <td colspan="3" align="left">2.75</td>
                      </tr>
                      <tr>
                        <td>Nilai</td>
                        <td>:</td>
                        <td>D</td>
                        <td>Minimal</td>
                        <td>:</td>
                        <td>2</td>
                      </tr>
                      <tr>
                        <td>Nilai E</td>
                        <td>:</td>
                         <td colspan="3" align="left">Tidak ada nilai E</td>
                      </tr>
                      
                    </table>
                  </div>

                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Hapus</button>
                  </div>

                </div>
              </div>
            </div>
                    <div class="semester-wrap">

                      <div class="top-bar">
                        <div class="row">
                          <div class="col-md-10 col-sm-10 col-xs-10 title">Semester 1</div>
                          <div class="col-md-2 col-sm-2 col-xs-2">
                            <div class="right">
                              <button data-toggle="modal" data-target=".hapus-semester" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            </div>
                          </div>
                        </div>
                      </div>


                      <table id="" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Kode Mata Kuliah</th>
                            <th>Nama Matakuliah</th>
                            <th>SKS</th>
                            <th>Prasyarat</th>
                            <th>&nbsp;</th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>MPK-1202</td>
                            <td>AIK 1 (Kemanusiaan dan Keimanan)</td>
                            <td>2/0</td>
                            <td>Tidak Ada</td>
                            <td>
                              <button data-toggle="modal" data-target=".tambah-prasyarat" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> Tambah</button>
                              <button data-toggle="modal" data-target=".hapus-matkul" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                          </tr>

                          <tr>
                            <td>2</td>
                            <td>MPK-1201</td>
                            <td>Aplikasi Sistem Komputer Farmasi</td>
                            <td>2/0</td>
                            <td>Tidak Ada</td>
                            <td>
                              <button data-toggle="modal" data-target=".tambah-prasyarat" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> Tambah</button>
                              <button data-toggle="modal" data-target=".hapus-matkul" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                          </tr>

                          <tr>
                            <td>2</td>
                            <td>MPK-1201</td>
                            <td>Aplikasi Sistem Komputer Farmasi</td>
                            <td>2/0</td>
                            <td><button data-toggle="modal" data-target=".prasyarat-matkul" class="btn btn-xs">Prasayarat</button></td>
                            <td>
                              <button data-toggle="modal" data-target=".tambah-prasyarat" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> Tambah</button>
                              <button data-toggle="modal" data-target=".hapus-matkul" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                          </tr>

                        </tbody>
                        <tfoot class="grey">
                          <tr>
                            <td colspan="3">Jumlah SKS Semester 1</td>
                            <td>7</td>
                            <td colspan="2">&nbsp;</td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>