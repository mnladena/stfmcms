<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Notifikasi</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Notifikasi</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

              <div class="mt15">
                    
                    <table class="datatable table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <input type="checkbox" id="check-all" class="flat">
                            </th>
                            <th class="column-title">Notifikasi </th>
                            <th class="column-title">Tanggal</th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr class="even pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 23, 2014 11:47:56 PM </td>
                            </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 23, 2014 11:30:12 PM</td>
                            </td>
                          </tr>
                          <tr class="even pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 24, 2014 10:55:33 PM</td>
                            </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 24, 2014 10:52:44 PM</td>
                            </td>
                          </tr>
                          <tr class="even pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 24, 2014 11:47:56 PM </td>
                            </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 26, 2014 11:30:12 PM</td>
                            </td>
                          </tr>
                          <tr class="even pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 26, 2014 10:55:33 PM</td>
                            </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 26, 2014 10:52:44 PM</td>
                            </td>
                          </tr>

                          <tr class="even pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 27, 2014 11:47:56 PM </td>
                            </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records">
                            </td>
                            <td class=" ">
                              <h5>Jadwal Kuliah</h5>
                              Jadwal Kuliah Sudah Diupdate
                            </td>
                            <td class=" ">May 28, 2014 11:30:12 PM</td>
                            </td>
                          </tr>
                        </tbody>
                      </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>