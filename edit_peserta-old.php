<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kegiatan PMB - Tahun 2019 - Gelombang 2</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Penilaian PMB</li>
                    </ol>
                </div>
              </div>

          </div>

          <div class="clearfix"></div>

          <div class="mt15">

            <div class="" role="tabpanel" data-example-id="togglable-tabs">

              <ul id="interview-tab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Biodata</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Orang Tua</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Referensi</a>
                </li>
              </ul>

              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="aik-tab">
                  
                  <div class="mt15">

                  <form id="" data-parsley-validate class="form-horizontal form-label-left">

                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Pendaftaran
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">Regular</option>
                          <option value="2">Non-Regular</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenjang
                      </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input required="required" type="radio" class="flat left" checked value="jenjang1" id="jenjang1" name="jenjang"> <span class="inp-text left">D3 Farmasi</span>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input required="required" type="radio" class="flat left" value="jenjang2" id="jenjang2" name="jenjang"> <span class="inp-text left">S1 Farmasi</span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="Mahathir Mohammad">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Kelamin
                      </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input type="radio" class="flat left" checked value="jenis1" id="jenis1" name="jenis"> <span class="inp-text left">Laki-laki</span>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input type="radio" class="flat left" value="jenis2" id="jenis2" name="jenis"> <span class="inp-text left">Perempuan</span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tempat/Tanggal Lahir
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="Banjarmasin">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class='input-group date'>
                            <input required="required" type='text' class="form-control"  id='datetimepicker7' value="20/08/1995"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Agama
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">Islam</option>
                          <option value="2">Kristen</option>
                          <option value="3">Buddha</option>
                          <option value="4">Hindu</option>
                          <option value="5">Kepercayaan</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat KTP
                      </label>
                      <div class="col-md-5 col-sm-5 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="Jalan Jalak Harupat No.20 RT01/RW02">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select required="required" id="" class="form-control">
                            <option value="">Choose..</option>
                            <option selected value="1">Kota Bogor</option>
                            <option value="2">Kabupaten Bogor</option>
                            <option value="3">Jakarta</option>
                            <option value="4">Tangerang</option>
                            <option value="5">Bekasi</option>
                          </select>
                        </div>
                    </div>
                        
                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select required="required" id="" class="form-control">
                            <option value="">Choose..</option>
                            <option selected value="1">Kecamatan Bogor Timur</option>
                            <option value="2">Kabupaten Bogor</option>
                            <option value="3">Jakarta</option>
                            <option value="4">Tangerang</option>
                            <option value="5">Bekasi</option>
                          </select>
                        </div>
                      </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <select required="required" id="" class="form-control">
                            <option value="">Choose..</option>
                            <option selected value="1">Kelurahan Loji</option>
                            <option value=2"">Kabupaten Bogor</option>
                            <option value="3">Jakarta</option>
                            <option value="4">Tangerang</option>
                            <option value="5">Bekasi</option>
                          </select>
                        </div>
                      </div>

                    <div class="form-group">
                      <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                          <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="16610">
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat Tempat Tinggal
                      </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="radio pilihan-jawaban">
                              <input type="radio" class="flat left" checked value="alamat1" id="alamat1" name="alamat"> <span class="inp-text left">Sesuai KTP</span>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="radio pilihan-jawaban">
                          <input type="radio" class="flat left" value="alamat2" id="alamat2" name="alamat"> <span class="inp-text left">Berbeda dari alamat KTP</span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Kewarganegaraan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">WNI</option>
                          <option value="2">WNA</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sekolah Asal
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">SMA</option>
                          <option value="2">SMK</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="SMAN 1 Bogor">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Status Pekerjaan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">Siswa</option>
                          <option value="2">Karyawan</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Golongan Darah
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">AB</option>
                          <option value="2">B</option>
                          <option value="3">O</option>
                          <option value="4">AB</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor HP
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="08112345667">
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
                    </div>

                    </form>
                  </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="akademik-tab">
                  
                  <div class="mt15">

                    <form id="" data-parsley-validate class="form-horizontal form-label-left">

                    <div class="form-group">
                        <h4 class="control-label col-md-3 col-sm-3 col-xs-12">Ayah
                        </h4>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="Herman Kurniwan">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pekerjaan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">Wiraswasta</option>
                          <option value="2">Petani</option>
                          <option value="3">Karyawan</option>
                          <option value="4">Driver</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor HP
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="08112345667">
                      </div>
                    </div>

                    <div class="form-group">
                        <h4 class="control-label col-md-3 col-sm-3 col-xs-12">Ibu
                        </h4>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="Sulistianingsih">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pekerjaan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">Ibu Rumah Tangga</option>
                          <option value="2">Petani</option>
                          <option value="3">Karyawan</option>
                          <option value="4">Driver</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor HP
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="08112345667">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Penghasilan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="required" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1"> <= 1 Juta</option>
                          <option value="2"> 1 Juta - 5 Juta</option>
                          <option value="3">5 Juta - 10 Juta</option>
                          <option value="4"> > 10 Juta </option>
                        </select>
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
                    </div>

                    </form>

                  </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="keuangan-tab">
                   
                  <div class="mt15">
                    <h3>Referensi</h3>

                    <form id="" data-parsley-validate class="form-horizontal form-label-left">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Informasi STF dari
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <select required="" id="" class="form-control">
                          <option value="">Choose..</option>
                          <option selected value="1">Media Online</option>
                          <option value="2">Keluarga</option>
                          <option value="3">Teman</option>
                          <option value="4">Iklan</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Referal
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input required="required" type="text" id="" class="form-control col-md-7 col-xs-12" value="detik.com">
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
                    </div>

                    </form>

                  </div>
                </div>

            </div>
          </div>
          

            
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>