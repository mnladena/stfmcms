<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kepala Prodi S1</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="#">Program Studi</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Kepala Prodi</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-9 col-sm-9 col-xs-9">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-kaprodi"><i class="fa fa-plus-circle"></i> Tambah Kepala Program Studi</button>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 right">
                <a href="pengaturan_prodi.php" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
              </div>
            </div>

            <div class="mt15">

            <!-- tambah kaProdi  -->
            <div class="modal fade tambah-kaprodi" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Kepala Program Studi</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Ana Yuliana </option>
                            <option value="">Asti Ananda </option>
                            <option value="">Azhari Manahan </option>
                            <option value="">Bambang Rudianto </option>
                            <option value="">Bisma Suryana </option>
                            <option value="">Cecep Sanusi </option>
                            <option value="">Clarisa Oktaviana </option>
                            <option value="">Derry Gumilang </option>
                            <option value="">Dina Mardiana </option>
                            <option value="">Elliana Haryati </option>
                            <option value="">Fahmi Idris </option>
                            <option value="">Faisal Hasan Jumadil </option>
                            <option value="">Ginanda Saras Hanum </option>
                            <option value="">Hasan Ryan Mandalangi </option>
                            <option value="">Lestika Putri </option>
                            <option value="">Monica Hardjo </option>
                            <option value="">Oscar Hamilton </option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Status
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <div class="checkbox">
                                <label>
                                  <input type="checkbox" class="flat"> Aktif
                                </label>
                              </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit kaProdi  -->
            <div class="modal fade edit-kaprodi" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-med">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Kepala Program Studi</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nama
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="" class="form-control">
                            <option value="">Choose..</option>
                            <option value="">Ana Yuliana </option>
                            <option value="">Asti Ananda </option>
                            <option value="">Azhari Manahan </option>
                            <option value="">Bambang Rudianto </option>
                            <option value="">Bisma Suryana </option>
                            <option value="">Cecep Sanusi </option>
                            <option value="">Clarisa Oktaviana </option>
                            <option value="">Derry Gumilang </option>
                            <option value="">Dina Mardiana </option>
                            <option value="">Elliana Haryati </option>
                            <option value="">Fahmi Idris </option>
                            <option value="">Faisal Hasan Jumadil </option>
                            <option value="">Ginanda Saras Hanum </option>
                            <option value="">Hasan Ryan Mandalangi </option>
                            <option selected value="">Lestika Putri </option>
                            <option value="">Monica Hardjo </option>
                            <option value="">Oscar Hamilton </option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Status
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <div class="checkbox">
                                <label>
                                  <input type="checkbox" class="flat"checked> Aktif
                                </label>
                              </div>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-danger">Semua Kolom harus diisi</div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            

                    <table id="" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Kepala Program Studi</th>
                          <th>Pendidikan Terakhir</th>
                          <th>Status</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Meta Safitri S.Farm M.Sc APT</td>
                          <td>S2 -  Farmasi </td>
                          <td>Aktif</td>
                          <td>
                              <button data-toggle="modal" data-target=".edit-kaprodi" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</button>
                          </td>
                        </tr>
 
                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>