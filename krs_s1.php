<?php include "include/head.php" ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
          <!-- logo -->
          <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <img src="images/logo.png" alt=""> <span>STFM</span></a>
            </div>
            <div class="clearfix"></div>
           <!-- logo -->

            <?php include "include/profile.php" ?>

            <?php include "include/sidemenu.php" ?>

            <?php include "include/topmenu.php" ?>

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="">
            <div class="page-title">

              <div class="title_left">
                <h3>Kartu Rencana Studi S1</h3>
              </div>

              <div class="title_right">
                <div class="pull-right">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item" aria-current="page"><a href="index.php">Halaman Utama</a></li>
                      <li class="breadcrumb-item" aria-current="page"><a href="krs_s1.php">Akademik</a></li>
                      <li class="breadcrumb-item active" aria-current="page">KRS - S1</li>
                    </ol>
                </div>
              </div>

          </div>

            <div class="clearfix"></div>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".tambah-tahun"><i class="fa fa-plus-circle"></i> Tambah Tahun Ajaran</button>

            <div class="mt15">

            <!-- create  -->
            <div class="modal fade tambah-tahun" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Tambah Tahun Ajaran</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">

                      <div class="form-group">
                        <label class="control-label" for="">Tahun Ajaran
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label" for="">Status
                        </label>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" class="flat"> Aktif
                            </label>
                          </div>
                      </div>

                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

            <!-- edit -->
            <div class="modal fade edit-tahun" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Tahun Ajaran</h4>
                  </div>
                  <div class="modal-body">
                    <form id="" class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label" for="">Tahun Ajaran
                        </label>
                        <div class="">
                          <input type="text" id="" class="form-control" value="2019/2020 - Ganjil">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label" for="">Status
                        </label>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" class="flat" checked> Aktif
                            </label>
                          </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer center ">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                  </div>

                </div>
              </div>
            </div>

                    <table id="" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tahun Ajaran</th>
                          <th>Status</th>
                          <th class="no-sort">Action</th>
                        </tr>
                      </thead>


                      <tbody>

                      <?php for ($i = 0; $i < 6; $i++){ 
                      $j=$i+1;
                      echo '
                        <tr>
                          <td>'.$j.'</td>
                          <td>2019/2020 - Ganjil</td>
                          <td>Aktif</td>
                          <td>
                            <a href="detail_krs.php" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Detail</a>
                            <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".edit-tahun"><i class="fa fa-edit"></i> Edit Tahun Ajaran</button>
                          </td>
                        </tr>';
                      }?>

                      </tbody>
                    </table>
            
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php include "include/footer.php" ?>

<!-- Initialize datetimepicker -->
<script>
    
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker6').datetimepicker({
      format: 'DD/MM/YYYY'
    });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY', 
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

        // $("#upload").dropzone({ url: "/file/post" });
</script>